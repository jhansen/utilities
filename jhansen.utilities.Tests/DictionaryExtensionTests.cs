﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace JHansen.Utilities.Tests
{
	/// <summary>
	/// Summary description for DictionaryExtensionTests
	/// </summary>
	[TestFixture]
	public class DictionaryExtensionTests
	{
		[Test]
		public void TryGetValue_DictionaryStringString_GetsValueAndType()
		{
			// Arrange
			Dictionary<string, string> dict = new Dictionary<string, string> { ["key"] = "value" };

			// Act
			object value;
			Type valueType;
			bool result = dict.TryGetValue("key", null, false, out value, out valueType);

			// Assert
			Assert.IsTrue(result);
			Assert.AreEqual(dict["key"], value);
			Assert.AreEqual(typeof(string), valueType);
		}

		[Test]
		public void TryGetValue_DictionaryStringString_PassDictType_GetsValueAndType()
		{
			// Arrange
			Dictionary<string, string> dict = new Dictionary<string, string> { ["key"] = "value" };

			// Act
			object value;
			Type valueType;
			bool result = dict.TryGetValue("key", dict.GetType(), false, out value, out valueType);

			// Assert
			Assert.IsTrue(result);
			Assert.AreEqual(dict["key"], value);
			Assert.AreEqual(typeof(string), valueType);
		}

		[Test]
		public void TryGetValue_DerivedDictionary_GetsValueAndType()
		{
			// Arrange
			DerivedDictionary dd = new DerivedDictionary {["key"] = "value"};

			// Act
			object value;
			Type valueType;
			bool result = dd.TryGetValue("key", null, false, out value, out valueType);

			// Assert
			Assert.IsTrue(result);
			Assert.AreEqual(dd["key"], value);
			Assert.AreEqual(typeof (string), valueType);
		}

		[Test]
		public void TryGetValue_DerivedDictionary_NonExistentKey_ReturnsFalse()
		{
			// Arrange
			DerivedDictionary dd = new DerivedDictionary { ["key"] = "value" };

			// Act
			object value;
			Type valueType;
			bool result = dd.TryGetValue("fakefakefake", null, false, out value, out valueType);

			// Assert
			Assert.IsFalse(result);
			Assert.IsNull(value);
			Assert.AreEqual(typeof(object), valueType);
		}

		[Test]
		public void TryGetValue_DerivedDictionary_NonExistentKey_CreateChain_SetsValue()
		{
			// Arrange
			DerivedDictionary dd = new DerivedDictionary { ["key"] = "value" };

			// Act
			object value;
			Type valueType;
			bool result = dd.TryGetValue("newkey", null, true, out value, out valueType);

			// Assert
			Assert.IsTrue(result);
			Assert.IsTrue(dd.ContainsKey("newkey"));
			Assert.AreEqual(dd["newkey"], value);
			Assert.AreEqual(typeof(object), valueType);
		}
	}

	public class DerivedDictionary : Dictionary<string, object>
	{
		
	}
}
