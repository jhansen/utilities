﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using JHansen.Utilities.Reflection;
using JHansen.Utilities.StateManagement;
using NUnit.Framework;

namespace JHansen.Utilities.Tests
{
	[TestFixture]
	public class ObjectExtensionsTests
	{
		[Test]
		public void GetPropertyEvalInfo_FakePropertyName_ReturnsNulls()
		{
			// arrange
			var instance = new Foo { PropA = "bar" };
			string prop = "Foo.IDontExist";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.IsNull(info.Parent);
			Assert.IsNull(info.GetValue());
			Assert.IsNull(info.ValueType);
			Assert.IsFalse(info.SuccessfullyEvaluated);
		    Assert.AreEqual(ReflectedMemberType.None, info.MemberType);
		}
		
		[Test]
	    public void GetPropertyEvalInfo_Expando_FakePropertyName_ReturnsNulls()
	    {
	        // arrange
	        var foo = new Foo { PropA = "bar" };
	        var dict = new Dictionary<string, object> { { nameof(Foo), foo } };
	        dynamic instance = JsonConvert.DeserializeObject<ExpandoObject>(JsonConvert.SerializeObject(dict));
	        string prop = "Foo.IDontExist";

            // act
            PropertyEvaluationInfo info = ((object)instance).GetPropertyEvalInfo(prop);

	        // assert
	        Assert.IsNull(info.Parent);
	        Assert.IsNull(info.GetValue());
	        Assert.IsNull(info.ValueType);
	        Assert.IsFalse(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.None, info.MemberType);
	    }

		[Test]
		public void GetPropertyEvalInfo_FakeClassName_ReturnsNulls()
		{
			// arrange
			var instance = new Foo { PropA = "bar" };
			string prop = "IDontExist.Blah";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.IsNull(info.Parent);
			Assert.IsNull(info.GetValue());
			Assert.IsNull(info.ValueType);
			Assert.IsFalse(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.None, info.MemberType);
        }
		
		[Test]
	    public void GetPropertyEvalInfo_Expando_FakeClassName_ReturnsNulls()
	    {
            // arrange
	        var foo = new Foo { PropA = "bar" };
	        var dict = new Dictionary<string, object> { { nameof(Foo), foo } };
	        dynamic instance = JsonConvert.DeserializeObject<ExpandoObject>(JsonConvert.SerializeObject(dict));
            string prop = "IDontExist.Blah";

	        // act
	        PropertyEvaluationInfo info = ((object)instance).GetPropertyEvalInfo(prop);

	        // assert
	        Assert.IsNull(info.Parent);
	        Assert.IsNull(info.GetValue());
	        Assert.IsNull(info.ValueType);
	        Assert.IsFalse(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.None, info.MemberType);
	    }

		[Test]
		public void GetPropertyEvalInfo_TopLevelProperty_GetsInfo()
		{
			// arrange
			var instance = new Foo { PropA = "bar" };
			string prop = "Foo.PropA";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreSame(instance, info.Parent);
			Assert.AreEqual(instance.PropA, info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }
		
		[Test]
	    public void GetPropertyEvalInfo_ExpandoWithoutTypeNames_TopLevelProperty_GetsInfo()
	    {
            // arrange
	        var foo = new Foo { PropA = "bar" };
	        var dict = new Dictionary<string, object> {{nameof(Foo), foo}};
	        dynamic instance = JsonConvert.DeserializeObject<ExpandoObject>(JsonConvert.SerializeObject(dict));
            string prop = "Foo.PropA";

	        // act
	        PropertyEvaluationInfo info = ((object)instance).GetPropertyEvalInfo(prop);

	        // assert
	        Assert.AreSame(instance.Foo, info.Parent);
	        Assert.AreEqual(instance.Foo.PropA, info.GetValue());
	        Assert.AreEqual(typeof(string), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Dictionary, info.MemberType);
	    }

	    [Test]
	    public void GetPropertyEvalInfo_ExpandoWithoutTypeNames_AccessByIndexer_TopLevelProperty_GetsInfo()
	    {
	        // arrange
	        var foo = new Foo { PropA = "bar" };
	        var dict = new Dictionary<string, object> { { nameof(Foo), foo } };
	        dynamic instance = JsonConvert.DeserializeObject<ExpandoObject>(JsonConvert.SerializeObject(dict));
	        string prop = "Foo[PropA]";

	        // act
	        PropertyEvaluationInfo info = ((object)instance).GetPropertyEvalInfo(prop);

	        // assert
	        Assert.AreSame(instance.Foo, info.Parent);
	        Assert.AreEqual(instance.Foo.PropA, info.GetValue());
	        Assert.AreEqual(typeof(string), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Dictionary, info.MemberType);
	    }

	    [Test]
	    public void GetPropertyEvalInfo_ExpandoWithoutTypeNames_TopLevelProperty_CasingDifference_GetsInfo()
	    {
	        // arrange
	        var foo = new Foo { PropA = "bar" };
	        var dict = new Dictionary<string, object> { { nameof(Foo), foo } };
	        dynamic instance = JsonConvert.DeserializeObject<ExpandoObject>(JsonConvert.SerializeObject(dict));
	        string prop = "Foo.PROPA";

	        // act
	        PropertyEvaluationInfo info = ((object)instance).GetPropertyEvalInfo(prop);

	        // assert
	        Assert.AreSame(instance.Foo, info.Parent);
	        Assert.AreEqual(instance.Foo.PropA, info.GetValue());
	        Assert.AreEqual(typeof(string), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Dictionary, info.MemberType);
	    }

	    [Test]
	    public void GetPropertyEvalInfo_ExpandoWithoutTypeNames_AccessByIndexer_TopLevelProperty_CasingDifference_GetsInfo()
	    {
	        // arrange
	        var foo = new Foo { PropA = "bar" };
	        var dict = new Dictionary<string, object> { { nameof(Foo), foo } };
	        dynamic instance = JsonConvert.DeserializeObject<ExpandoObject>(JsonConvert.SerializeObject(dict));
	        string prop = "Foo[PROPA]";

	        // act
	        PropertyEvaluationInfo info = ((object)instance).GetPropertyEvalInfo(prop);

	        // assert
	        Assert.AreSame(instance.Foo, info.Parent);
	        Assert.AreEqual(instance.Foo.PropA, info.GetValue());
	        Assert.AreEqual(typeof(string), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Dictionary, info.MemberType);
	    }

        [Test]
	    public void GetPropertyEvalInfo_ExpandoWithTypeNames_TopLevelProperty_GetsInfo()
	    {
	        // arrange
	        var foo = new Foo { PropA = "bar" };
	        var dict = new Dictionary<string, object> { { nameof(Foo), foo } };
	        string serialized =
	            JsonConvert.SerializeObject(dict, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
	        dynamic instance = JsonConvert.DeserializeObject<ExpandoObject>(
	            serialized,
	            new JsonSerializerSettings
	            {
	                TypeNameHandling = TypeNameHandling.All,
	                Converters = new[] { new TypeNameHandlingExpandoObjectConverter() }
	            });
	        string prop = "Foo.PropA";

	        // act
	        PropertyEvaluationInfo info = ((object)instance).GetPropertyEvalInfo(prop);

	        // assert
	        Assert.AreSame(instance.Foo, info.Parent);
	        Assert.AreEqual(instance.Foo.PropA, info.GetValue());
	        Assert.AreEqual(typeof(string), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
	    }

	    [Test]
	    public void GetPropertyEvalInfo_ExpandoWithInvalidTypeNames_TopLevelProperty_GetsInfo()
	    {
	        // arrange
            // this is the JSON of an object whose type doesn't exist in this AppDomain
	        string serialized = @"{    
    ""ExperimentReferral"":{""$type"":""External.Services.ExperimentReferral,External.Services"",
	    ""AppName"": ""SafeliteDotcom"",
	    ""SelectedPassengerSideGlassCategory"": false,
	    ""ProviderNumber"": ""00000000"",
	    ""ParentNum"": 167132
    }
}";

	        dynamic instance = JsonConvert.DeserializeObject<ExpandoObject>(
	            serialized,
	            new JsonSerializerSettings
	            {
	                TypeNameHandling = TypeNameHandling.All,
	                Converters = new[] { new TypeNameHandlingExpandoObjectConverter() }
	            });
	        string prop = "ExperimentReferral.ParentNum";

	        // act
	        PropertyEvaluationInfo info = ((object)instance).GetPropertyEvalInfo(prop);

	        // assert
	        Assert.AreSame(instance.ExperimentReferral, info.Parent);
	        Assert.AreEqual(typeof(ExpandoObject), info.Parent.GetType());
	        Assert.AreEqual(instance.ExperimentReferral.ParentNum, info.GetValue());
	        Assert.AreEqual(typeof(long), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Dictionary, info.MemberType);
	    }

		[Test]
		public void GetPropertyEvalInfo_TopLevelProperty_CanSetValue()
		{
			// arrange
			var instance = new Foo { PropA = "bar" };
			string prop = "Foo.PropA";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.IsTrue(info.CanSetValue);
			Assert.IsTrue(info.SetValue("new bar"));
			Assert.AreEqual("new bar", instance.PropA);
		}
		
		[Test]
	    public void GetPropertyEvalInfo_Expando_TopLevelProperty_CanSetValue()
	    {
            // arrange
	        var foo = new Foo { PropA = "bar" };
	        var dict = new Dictionary<string, object> { { nameof(Foo), foo } };
	        dynamic instance = JsonConvert.DeserializeObject<ExpandoObject>(JsonConvert.SerializeObject(dict));
	        string prop = "Foo.PropA";

            // act
            PropertyEvaluationInfo info = ((object)instance).GetPropertyEvalInfo(prop);

	        // assert
	        Assert.IsTrue(info.CanSetValue);
	        Assert.IsTrue(info.SetValue("new bar"));
	        Assert.AreEqual("new bar", instance.Foo.PropA);
	    }

		[Test]
		public void GetPropertyEvalInfo_TopLevelMethod_NoParameters_GetsInfo()
		{
			// arrange
			var instance = new Foo { PropA = "bar" };
			string prop = "Foo.GetPropA()";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreSame(instance, info.Parent);
			Assert.AreEqual(instance.GetPropA(), info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Method, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_TopLevelMethod_NoParameters_CantSetValue()
		{
			// arrange
			var instance = new Foo { PropA = "bar" };
			string prop = "Foo.GetPropA()";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.IsFalse(info.CanSetValue);
			Assert.IsFalse(info.SetValue("new bar"));
			Assert.AreEqual("bar", instance.GetPropA());
		}

		[Test]
		public void GetPropertyEvalInfo_NonExistentMethod_NoParameters_ReturnsUnsuccessful()
		{
			// arrange
			var instance = new Foo();
			string prop = "Foo.IDontExist()";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreSame(instance, info.Parent);
			Assert.IsNull(info.GetValue());
			Assert.IsFalse(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.None, info.MemberType);
        }

        [Test]
        public void GetPropertyEvalInfo_GenericExtensionMethod_GetsInfo()
        {
            // arrange
            Bar bar = new Bar {NestedProperty = 123};
            var instance = new Foo{PropA = JsonConvert.SerializeObject(bar)};
            string prop = "Foo.DeserializePropA<Bar>()";

            // act
            PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

            // assert
            object value = info.GetValue();
            Assert.AreEqual(bar, value);
            Assert.AreEqual(typeof(Bar), info.ValueType);
            Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Method, info.MemberType);
        }
		
		[Test]
	    public void GetPropertyEvalInfo_Expando_GenericExtensionMethod_GetsInfo()
	    {
	        // arrange
	        Bar bar = new Bar { NestedProperty = 123 };
	        Foo foo = new Foo { PropA = JsonConvert.SerializeObject(bar) };
	        Dictionary<string, object> dict = new Dictionary<string, object> {{nameof(Foo), foo}};
	        dynamic instance = JsonConvert.DeserializeObject<ExpandoObject>(JsonConvert.SerializeObject(dict));
	        string prop = "Foo.PropA.DeserializeString<Bar>()";

	        // act
	        PropertyEvaluationInfo info = ((object)instance).GetPropertyEvalInfo(prop);

	        // assert
	        object value = info.GetValue();
	        Assert.AreEqual(bar, value);
	        Assert.AreEqual(typeof(Bar), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Method, info.MemberType);
	    }

		[Test]
		public void GetPropertyEvalInfo_TopLevelMethod_StringParameter_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			string prop = "Foo.Reverse(mit)";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreSame(instance, info.Parent);
			Assert.AreEqual("tim", info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Method, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_TopLevelMethod_StringParameters_GetsInfo()
		{
			// arrange
			var instance = new Foo{ PropA = "Timothy"};
			string prop = "Foo.PropA.Replace(oth,)";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreSame(instance.PropA, info.Parent);
			Assert.AreEqual("Timy", info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
			Assert.AreEqual(ReflectedMemberType.Method, info.MemberType);
		}

		[Test]
		public void GetPropertyEvalInfo_MethodResultSubProperty_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2 });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = "Foo.GetBang(1).Data"; // index: 1 == Id: 2

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Bang bang = instance.Bangs.First(b => b.Id == 2);
			Assert.AreSame(bang, info.Parent);
			Assert.AreEqual(bang.Data, info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

        [Test]
        public void GetPropertyEvalInfo_NestedGenericMethod_GetsInfo()
        {
            // arrange
            var instance = new Foo();
            instance.Bangs.Add(new Bang { Id = 1 });
            instance.Bangs.Add(new Bang { Id = 2, Fizz = new Fizz() });
            instance.GetBang(1).Fizz.Buzz = JsonConvert.SerializeObject(instance);
            string prop = "Foo.GetBang(1).DeserializeBuzz<Foo>()"; 

            // act
            PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

            // assert
            Bang bang = instance.Bangs.First(b => b.Id == 2);
            Assert.AreSame(bang, info.Parent);
            Assert.AreEqual(typeof(Foo), info.ValueType);
            Assert.AreEqual(instance.Bangs[0].Id, ((Foo)info.GetValue()).Bangs[0].Id);
            Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Method, info.MemberType);
        }

        [Test]
		public void GetPropertyEvalInfo_MethodResultDictListSubProperty_GetsInfo()
		{
			// arrange
			var instance = new Foo
			{
				DictOfLists =
				{
					["key"] = new List<Bang>
					{
						new Bang {Id = 1},
						new Bang {Id = 2},
						new Bang {Id = 3}
					}
				}
			};
			string prop = @"Foo.GetListOfDicts()[key]{Id == 2}.Data";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Bang bang = instance.DictOfLists["key"].First(b => b.Id == 2);
			Assert.AreSame(bang, info.Parent);
			Assert.AreEqual(bang.Data, info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_NestedExtensionMethodNoParameters_GetsInfo()
		{
			// arrange
			var instance = new Foo { DateProp = DateTime.UtcNow };
			string prop = "Foo.DateProp.Foormat()";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreEqual(instance.DateProp, info.Parent);
			Assert.AreEqual(instance.DateProp.Foormat(), info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Method, info.MemberType);
        }

        [Test]
        public void GetPropertyEvalInfo_NestedExtensionMethod_CanSet()
        {
            // arrange
            var instance = new Foo { DateProp = DateTime.UtcNow };
            string prop = "Foo.DateProp.Foormat()";

            // act
            PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

            // assert
            Assert.IsTrue(info.CanSetValue);
            DateTime newValue = DateTime.UtcNow.AddDays(1);
            info.SetValue(newValue); // setting the value when the end of the reflection string is a method sets the parent object
            Assert.AreEqual(newValue, instance.DateProp);
        }

        [Test]
		public void GetPropertyEvalInfo_NestedExtensionMethodOverloadWithParameter_GetsInfo()
		{
			// arrange
			var instance = new Foo { DateProp = DateTime.UtcNow };
			string prop = "Foo.DateProp.Foormat(MM/dd/yyyy)";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreEqual(instance.DateProp, info.Parent);
			Assert.AreEqual(instance.DateProp.Foormat("MM/dd/yyyy"), info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Method, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_ExtensionMethodPropertyOfResult_GetsInfo()
		{
			// arrange
			var instance = new Foo { NestedObject = { NestedProperty = 1337 } };
			string prop = "Foo.GetBar().NestedProperty";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreEqual(instance.GetBar(), info.Parent);
			Assert.AreEqual(instance.GetBar().NestedProperty, info.GetValue());
			Assert.AreEqual(typeof(int), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

        [Test]
        public void GetPropertyEvalInfo_NestedPropertyOfObject_GetsInfo()
        {
            // arrange
            var instance = new Foo { NestedBoxedObject = new Bar { NestedProperty = 1337 } };
            string prop = "Foo.NestedBoxedObject.NestedProperty";

            // act
            PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

            // assert
            Assert.AreEqual(instance.NestedBoxedObject, info.Parent);
            Assert.AreEqual(((Bar)instance.NestedBoxedObject).NestedProperty, info.GetValue());
            Assert.AreEqual(typeof(int), info.ValueType);
            Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_DatePropertyFormatString_GetsInfo()
		{
			// arrange
			var instance = new Foo { DateProp = DateTime.Now };
			string prop = "Foo.DateProp:MM/dd/yy HH:mm:ss..FF"; // NOTE: escape period in the format string with ".."

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreSame(instance, info.Parent);
			Assert.AreEqual(instance.DateProp.ToString("MM/dd/yy HH:mm:ss.FF"), info.GetValue());
			Assert.AreEqual(typeof(DateTime), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

        [Test]
        public void GetPropertyEvalInfo_StatefulNullableDatePropertyFormatString_GetsInfo()
        {
            // arrange
            var instance = new Foo { StatefulNullableDate = DateTime.Now };
            string prop = "Foo.StatefulNullableDate.Value.Value:MMMM dd, yyyy"; 

            // act
            PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

            // assert
            Assert.AreEqual(instance.StatefulNullableDate.Value.Value.ToString("MMMM dd, yyyy"), info.GetValue());
            Assert.AreEqual(typeof(DateTime), info.ValueType);
            Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_NestedObjectProperty_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			string prop = "Foo.NestedObject.NestedProperty";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreSame(instance.NestedObject, info.Parent);
			Assert.AreEqual(instance.NestedObject.NestedProperty, info.GetValue());
			Assert.AreEqual(typeof(int), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_NestedObjectNull_CreatesObjectGetsInfo()
		{
			// arrange
			var instance = new Foo {NestedObject = null};
			string prop = "Foo.NestedObject.NestedProperty";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop, createChain: true);

			// assert
			Assert.IsNotNull(instance.NestedObject);
			Assert.AreSame(instance.NestedObject, info.Parent);
			Assert.AreEqual(instance.NestedObject.NestedProperty, info.GetValue());
			Assert.AreEqual(typeof(int), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_DictItemString_GetValue_GetsString()
		{
			// arrange
			var instance = new Foo();
			instance.KeyedStrings["key"] = "Stacks";
			string prop = "Foo.KeyedStrings[key]";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreSame(instance.KeyedStrings["key"], info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Dictionary, info.MemberType);
        }
		
		[Test]
	    public void GetPropertyEvalInfo_DictItemString_CasingDifference_GetValue_GetsString()
	    {
	        // arrange
	        var instance = new Foo();
	        instance.KeyedStrings["key"] = "Stacks";
	        string prop = "Foo.KeyedStrings[KEY]";

	        // act
	        PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

	        // assert
	        Assert.AreSame(instance.KeyedStrings["key"], info.GetValue());
	        Assert.AreEqual(typeof(string), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Dictionary, info.MemberType);
	    }

		[Test]
		public void GetPropertyEvalInfo_DictItemString_SetValue_SetsDictItem()
		{
			// arrange
			var instance = new Foo();
			instance.KeyedStrings["key"] = "Stacks";
			string prop = "Foo.KeyedStrings[key]";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.IsTrue(info.CanSetValue);
			Assert.IsTrue(info.SetValue("new stacks"));
			Assert.AreEqual("new stacks", instance.KeyedStrings["key"]);
		}

		[Test]
		public void GetPropertyEvalInfo_DictItemString_NonExistentItem_GetsValueType()
		{
			// arrange
			var instance = new Foo();
			instance.KeyedStrings["key"] = "Stacks";
			string prop = "Foo.KeyedStrings[otherkey]";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.IsNull(info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsFalse(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Dictionary, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_DictItemProperty_GetsInfo()
		{
			// arrange
			var instance = new Foo ();
			instance.Dict["key"] = new Fizz {Buzz = "BuzzValue"};
			string prop = "Foo.Dict[key].Buzz";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreSame(instance.Dict["key"], info.Parent);
			Assert.AreEqual("BuzzValue", info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_DictItemDoesntExist_CreatesItemGetsInfo()
		{
			// arrange
			var instance = new Foo();
			string prop = "Foo.Dict[key].Buzz";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop, createChain: true);

			// assert
			Assert.AreSame(instance.Dict["key"], info.Parent);
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
		}

		[Test]
		public void GetPropertyEvalInfo_DictItemComplexObjectDoesntExist_SetValue_SetsItem()
		{
			// arrange
			var instance = new Foo();
			string prop = "Foo.Dict[key]";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop, createChain: true);

			// assert
			Assert.IsTrue(info.SuccessfullyEvaluated);
			Assert.IsTrue(info.CanSetValue);
			Assert.IsTrue(info.SetValue(new Fizz {Buzz = "biz"}));
			Assert.AreEqual("biz", instance.Dict["key"].Buzz);
            Assert.AreEqual(ReflectedMemberType.Dictionary, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_NonExistantDictItemProperty_ReturnsNulls()
		{
			// arrange
			var instance = new Foo();
			instance.Dict["key"] = new Fizz { Buzz = "BuzzValue" };
			string prop = "Foo.Dict[notarealkey].Buzz";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.IsNull(info.Parent);
			Assert.IsNull(info.GetValue());
			Assert.IsNull(info.ValueType);
			Assert.IsFalse(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.None, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_DictItemProperty_QuotedKey_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			instance.Dict["key"] = new Fizz { Buzz = "BuzzValue" };
			string prop = @"Foo.Dict[""key""].Buzz";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreSame(instance.Dict["key"], info.Parent);
			Assert.AreEqual("BuzzValue", info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_QueryListByWhereString_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2 });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Id == 2}.Data";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Bang bang = instance.Bangs.First(b => b.Id == 2);
            Assert.AreSame(bang, info.Parent);
			Assert.AreEqual(bang.Data, info.GetValue());
			Assert.AreEqual(typeof (string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_GetFilteredListByWhereString_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2 });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Id > 1}[]";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			object value = info.GetValue();
			Assert.IsNotNull(value);
			Assert.IsTrue(typeof(IEnumerable<object>).IsAssignableFrom(info.ValueType));
			IEnumerable<object> list = (IEnumerable<object>)value;
			Assert.IsNotNull(list);
			Assert.AreEqual(2, list.Count());
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.FilteredList, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_GetEmptyListByWhereString_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2 });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Id > 99}[]";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			object value = info.GetValue();
			Assert.IsNotNull(value);
			Assert.IsTrue(typeof(IEnumerable<object>).IsAssignableFrom(info.ValueType));
			IEnumerable<object> list = (IEnumerable<object>)value;
			Assert.AreEqual(0, list.Count());
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.FilteredList, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_QueryList_EndOnWhereString_GetValue_ReturnsObject()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2 });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Id == 2}";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Bang bang = instance.Bangs.First(b => b.Id == 2);
			Assert.AreSame(bang, info.GetValue());
			Assert.AreEqual(typeof (Bang), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.FilteredListItem, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_QueryListByWhereStringLiteral_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2, Fizz = { Buzz = "buzzbuzzbuzz" } });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Fizz.Buzz == ""buzzbuzzbuzz""}.Data";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Bang bang = instance.Bangs.First(b => b.Id == 2);
			Assert.AreSame(bang, info.Parent);
			Assert.AreEqual(bang.Data, info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_QueryListCheckAny_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2, Fizz = { Buzz = "buzzbuzzbuzz" } });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Fizz.Buzz == ""buzzbuzzbuzz""}[].Any()";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.AreEqual(true, info.GetValue());
			Assert.AreEqual(typeof(bool), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Method, info.MemberType);
        }

        [Test]
        public void GetPropertyEvalInfo_QueryListCheckCount_GetsInfo()
        {
            // arrange
            var instance = new Foo();
            instance.Bangs.Add(new Bang { Id = 1 });
            instance.Bangs.Add(new Bang { Id = 2, SNB = { Value = true }});
            instance.Bangs.Add(new Bang { Id = 3 });
            string prop = @"Foo.Bangs{SNB.Value == true}[].Count()";

            // act
            PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

            // assert
            Assert.AreEqual(1, info.GetValue());
            Assert.AreEqual(typeof(int), info.ValueType);
            Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Method, info.MemberType);
        }
		
		[Test]
	    public void GetPropertyEvalInfo_StatefulNullableBoolState_GetsInfo()
	    {
	        // arrange
	        var instance = new Bang { Id = 2, SNB = { Value = true, State = ValueState.Hidden } };
	        string prop = @"Bang.SNB.State";

	        // act
	        PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

	        // assert
	        Assert.AreEqual(ValueState.Hidden, info.GetValue());
	        Assert.AreEqual(typeof(ValueState?), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
	    }

        [Test]
		public void GetPropertyEvalInfo_QueryListNoMatch_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2, Fizz = { Buzz = "buzzbuzzbuzz" } });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Fizz.Buzz == ""ThisKeyDoesntExist""}.Data";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.IsNull(info.GetValue());
			Assert.IsNull(info.ValueType);
			Assert.IsFalse(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.None, info.MemberType);
        }

	    [Test]
	    public void GetPropertyEvalInfo_GetFirstItem_GetsInfo()
	    {
	        // arrange
	        var instance = new Foo();
	        instance.Bangs.Add(new Bang { Id = 1, Fizz = { Buzz = "buzzbuzzbuzz" }  });
	        instance.Bangs.Add(new Bang { Id = 2 });
	        instance.Bangs.Add(new Bang { Id = 3 });
	        string prop = @"Foo.Bangs.First<Bang>().Data";

	        // act
	        PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

	        // assert
	        //Assert.IsNull(info.GetValue());
	        //Assert.IsNull(info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        //Assert.AreEqual(ReflectedMemberType.None, info.MemberType);
	    }

        [Test]
		public void GetPropertyEvalInfo_QueryListByStatefulWhereStringLiteral_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2, Fizz = { Stateful = "buzzbuzzbuzz" } });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Fizz.Stateful.Value == ""buzzbuzzbuzz""}.Data";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Bang bang = instance.Bangs.First(b => b.Id == 2);
			Assert.AreSame(bang, info.Parent);
			Assert.AreEqual(bang.Data, info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }
        
        [Test]
	    public void GetPropertyEvalInfo_QueryListByCompoundWhereStringLiteral_GetsInfo()
	    {
	        // arrange
	        var instance = new Foo();
	        instance.Bangs.Add(new Bang { Id = 1 });
	        instance.Bangs.Add(new Bang { Id = 2, FizzyDic = {{"mikey", new Fizz { Buzz = "Valery" }}} });
	        instance.Bangs.Add(new Bang { Id = 3 });
	        string prop = @"Foo.Bangs{Id == 2 && FizzyDic[""mikey""].Buzz == ""Valery""}.Data";

	        // act
	        PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

	        // assert
	        Bang bang = instance.Bangs.First(b => b.Id == 2);
	        Assert.AreSame(bang, info.Parent);
	        Assert.AreEqual(bang.Data, info.GetValue());
	        Assert.AreEqual(typeof(string), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
	    }

	    [Test]
	    public void GetPropertyEvalInfo_QueryListByCompoundWhereStringInListLiteral_GetsInfo()
	    {
	        // arrange
	        var instance = new Foo();
	        instance.Bangs.Add(new Bang { Id = 1 });
	        instance.Bangs.Add(new Bang { Id = 2, FizzyDic = { { "mikey", new Fizz { Buzz = "Valery" } } } });
	        instance.Bangs.Add(new Bang { Id = 3 });
	        string prop = @"Foo.Bangs{Id == 2 && ""|valery|Not Valery|"".Contains(""|"" + FizzyDic[""mikey""].Buzz.ToLower() + ""|"")}.Data";

	        // act
	        PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

	        // assert
	        Bang bang = instance.Bangs.First(b => b.Id == 2);
	        Assert.AreSame(bang, info.Parent);
	        Assert.AreEqual(bang.Data, info.GetValue());
	        Assert.AreEqual(typeof(string), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
	    }

	    [Test]
	    public void GetPropertyEvalInfo_QueryListByWhereStringInListLiteralAsList_GetsInfo()
	    {
	        // arrange
	        var instance = new Foo();
	        instance.Bangs.Add(new Bang { Id = 1 });
	        instance.Bangs.Add(new Bang { Id = 2, FizzyDic = { { "mikey", new Fizz { Buzz = "Valery" } } } });
	        instance.Bangs.Add(new Bang { Id = 3 });
	        string prop = @"Foo.Bangs{FizzyDic.Any(Key == ""mikey"" && Value.Buzz == ""Valery"")}[]";

	        // act
	        PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

	        // assert
	        var value = info.GetValue();
            Bang bang = instance.Bangs.First(b => b.Id == 2);
	        Assert.AreSame(instance, info.Parent);
	        Assert.AreEqual(typeof(List<Bang>), info.ValueType);
	        Assert.AreEqual(bang, ((IEnumerable)value).Cast<object>().First());
            Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.FilteredList, info.MemberType);
        }

        [Test]
	    public void GetPropertyEvalInfo_QueryListByCompoundWhereStringNotInListLiteral_GetsInfo()
	    {
	        // arrange
	        var instance = new Foo();
	        instance.Bangs.Add(new Bang { Id = 1 });
	        instance.Bangs.Add(new Bang { Id = 2, FizzyDic = { { "mikey", new Fizz { Buzz = "Valery " } } } });
	        instance.Bangs.Add(new Bang { Id = 3 });
	        string prop = @"Foo.Bangs{Id == 2 && !""|Not Valery|Not Valery 2|"".Contains(""|"" + FizzyDic[""mikey""].Buzz.Trim() + ""|"")}.Data";

	        // act
	        PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

	        // assert
	        Bang bang = instance.Bangs.First(b => b.Id == 2);
	        Assert.AreSame(bang, info.Parent);
	        Assert.AreEqual(bang.Data, info.GetValue());
	        Assert.AreEqual(typeof(string), info.ValueType);
	        Assert.IsTrue(info.SuccessfullyEvaluated);
	        Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
	    }

        [Test]
		public void GetPropertyEvalInfo_QueryListByTokenizedWhereString_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2 });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Id == BangId}.Data";
			NamedParameters parameters = new NamedParameters {{"BangId", 2}};

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop, parameters);

			// assert
			Bang bang = instance.Bangs.First(b => b.Id == 2);
			Assert.AreSame(bang, info.Parent);
			Assert.AreEqual(bang.Data, info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_QueryListByTokenizedWhereStringWithoutPassingParameters_ReturnsNulls()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2 });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Id == BangId}.Data";

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.IsNull(info.Parent);
			Assert.IsNull(info.GetValue());
			Assert.IsNull(info.ValueType);
			Assert.IsFalse(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.None, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_QueryListByTokenizedWhereStringWithNestedDot_GetsInfo()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2, Fizz = { Buzz = "buzzbuzzbuzz"} });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Fizz.Buzz == BuzzString}.Data";
			NamedParameters parameters = new NamedParameters { { "BuzzString", "buzzbuzzbuzz" } };

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop, parameters);

			// assert
			Bang bang = instance.Bangs.First(b => b.Id == 2);
			Assert.AreSame(bang, info.Parent);
			Assert.AreEqual(bang.Data, info.GetValue());
			Assert.AreEqual(typeof(string), info.ValueType);
			Assert.IsTrue(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.Property, info.MemberType);
        }

		[Test]
		public void GetPropertyEvalInfo_QueryListByTokenizedWhereStringZeroMatches_ReturnsNulls()
		{
			// arrange
			var instance = new Foo();
			instance.Bangs.Add(new Bang { Id = 1 });
			instance.Bangs.Add(new Bang { Id = 2 });
			instance.Bangs.Add(new Bang { Id = 3 });
			string prop = @"Foo.Bangs{Id == BangId}.Data";
			NamedParameters parameters = new NamedParameters { { "BangId", 99 } };

			// act
			PropertyEvaluationInfo info = instance.GetPropertyEvalInfo(prop, parameters);

			// assert
			Assert.IsNull(info.Parent);
			Assert.IsNull(info.GetValue());
			Assert.IsNull(info.ValueType);
			Assert.IsFalse(info.SuccessfullyEvaluated);
            Assert.AreEqual(ReflectedMemberType.None, info.MemberType);
        }

        [Test]
        public void GetPropertyValue_FakePropertyName_ReturnsNulls()
        {
            // arrange
            var instance = new Foo { PropA = "bar" };
            string prop = "Foo.IDontExist";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.IsNull(value);
        }

        [Test]
        public void GetPropertyValue_FakeClassName_ReturnsNulls()
        {
            // arrange
            var instance = new Foo { PropA = "bar" };
            string prop = "IDontExist.Blah";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.IsNull(value);
        }

        [Test]
        public void GetPropertyValue_TopLevelProperty_PropertyNameOnly_GetsValue()
        {
            // arrange
            var instance = new Foo { PropA = "bar" };
            string prop = "PropA";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.AreEqual(instance.PropA, value);
        }

        [Test]
        public void GetPropertyValue_TopLevelProperty_GetsValue()
        {
            // arrange
            var instance = new Foo { PropA = "bar" };
            string prop = "Foo.PropA";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.AreEqual(instance.PropA, value);
        }

		[Test]
		public void GetPropertyValue_DictionaryObject_GetsValue()
		{
			// arrange
			var instance = new Foo { Dict = { { "key", new Fizz { Buzz = "value" } } } };
			string prop = "Foo.Dict[key]";

			// act
			var value = instance.GetPropertyValue(prop);

			// assert
			Assert.AreEqual(instance.Dict["key"], value);
		}

		[Test]
		public void GetPropertyValue_DictionaryString_GetsValue()
		{
			// arrange
			var instance = new Foo { KeyedStrings = { { "key", "value" } } };
			string prop = "Foo.KeyedStrings[key]";

			// act
			var value = instance.GetPropertyValue(prop);

			// assert
			Assert.AreEqual(instance.KeyedStrings["key"], value);
		}

		[Test]
        public void GetPropertyValue_NestedObject_PropertyNameOnly_GetsValue()
        {
            // arrange
            var instance = new Foo();
            string prop = "NestedObject";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.AreEqual(instance.NestedObject, value);
            Assert.AreEqual(instance.NestedObject.NestedProperty, ((Bar)value).NestedProperty);
        }

        [Test]
        public void GetPropertyValue_KeyValuePair_PropertyNameOnly_GetsValue()
        {
            // arrange
            var instance = new KeyValuePair<string, string>("Here is a key", "Here is a value");
            string prop1 = "Key";
            string prop2 = "Value";

            // act
            var value1 = instance.GetPropertyValue(prop1);
            var value2 = instance.GetPropertyValue(prop2);

            // assert
            Assert.AreEqual(instance.Key, value1);
            Assert.AreEqual(instance.Value, value2);
        }

        [Test]
        public void GetPropertyValue_KeyValuePair_PropertyNameOnlyNestedObject_GetsValue()
        {
            // arrange
            var instance = new KeyValuePair<string, Foo>("Here is a key", new Foo { PropA = "Here is PropA of value"});
            string prop1 = "Key";
            string prop2 = "Value.PropA";

            // act
            var value1 = instance.GetPropertyValue(prop1);
            var value2 = instance.GetPropertyValue(prop2);

            // assert
            Assert.AreEqual(instance.Key, value1);
            Assert.AreEqual(instance.Value.PropA, value2);
        }

        [Test]
        public void GetPropertyValue_NestedObject_GetsValue()
        {
            // arrange
            var instance = new Foo();
            string prop = "Foo.NestedObject";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.AreEqual(instance.NestedObject, value);
            Assert.AreEqual(instance.NestedObject.NestedProperty, ((Bar)value).NestedProperty);
        }

        [Test]
        public void GetPropertyValue_NestedObjectProperty_GetsValue()
        {
            // arrange
            var instance = new Foo();
            string prop = "Foo.NestedObject.NestedProperty";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.AreEqual(instance.NestedObject.NestedProperty, value);
        }

		[Test]
		public void IsZero_ZeroInt_True()
		{
			// Arrange
			// Act
			bool result = 0.IsZero();

			// Assert
			Assert.IsTrue(result);
		}

		[Test]
		public void IsZero_ZeroFloat_True()
		{
			// Arrange
			// Act
			bool result = 0F.IsZero();

			// Assert
			Assert.IsTrue(result);
		}

		[Test]
		public void IsZero_ZeroBoxedFloat_True()
		{
			// Arrange
			// Act
			bool result = ((object)0F).IsZero();

			// Assert
			Assert.IsTrue(result);
		}

		[Test]
		public void IsZero_ZeroDouble_True()
		{
			// Arrange
			// Act
			bool result = 0D.IsZero();

			// Assert
			Assert.IsTrue(result);
		}

		[Test]
		public void IsZero_PositiveInt_False()
		{
			// Arrange
			// Act
			bool result = 1.IsZero();

			// Assert
			Assert.IsFalse(result);
		}

		[Test]
		public void IsZero_PositiveBoxedInt_False()
		{
			// Arrange
			// Act
			bool result = ((object)1).IsZero();

			// Assert
			Assert.IsFalse(result);
		}

		[Test]
		public void IsZero_NegativeInt_False()
		{
			// Arrange
			// Act
			bool result = (-1).IsZero();

			// Assert
			Assert.IsFalse(result);
		}

		[Test]
		public void IsZero_BlankString_False()
		{
			// Arrange
			// Act
			bool result = "".IsZero();

			// Assert
			Assert.IsFalse(result);
		}

		[Test]
		public void IsZero_TextString_False()
		{
			// Arrange
			// Act
			bool result = "blah".IsZero();

			// Assert
			Assert.IsFalse(result);
		}

		[Test]
		public void IsZero_ZeroString_True()
		{
			// Arrange
			// Act
			bool result = "0".IsZero();

			// Assert
			Assert.IsTrue(result);
		}

		[Test]
		public void IsZero_NullString_False()
		{
			// Arrange
			// Act
			bool result = ((string)null).IsZero();

			// Assert
			Assert.IsFalse(result);
		}

		[Test]
		public void IsZero_DateTime_False()
		{
			// Arrange
			// Act
			bool result = DateTime.Now.IsZero();

			// Assert
			Assert.IsFalse(result);
		}
        
	}

	public class Foo
	{
		public Foo()
		{
			NestedObject = new Bar {NestedProperty = 38};
			Dict = new Dictionary<string, Fizz>();
			DictOfLists = new Dictionary<string, List<Bang>>();
			KeyedStrings = new Dictionary<string, string>();
			Bangs = new List<Bang>();
		    StatefulNullableDate = new Stateful<DateTime?>();
		}

		public string PropA { get; set; }
		public DateTime DateProp { get; set; }
        public Stateful<DateTime?> StatefulNullableDate { get; set; }
        public Bar NestedObject { get; set; }
        public object NestedBoxedObject { get; set; }
		public Dictionary<string, string> KeyedStrings { get; set; }
		public Dictionary<string, Fizz> Dict { get; set; }
		public Dictionary<string, List<Bang>> DictOfLists { get; set; }
		public List<Bang> Bangs { get; set; }

		public string GetPropA()
		{
			return PropA;
		}

		public string Reverse(string toReverse)
		{
			return new string(toReverse.ToArray().Reverse().ToArray());
		}

		public Bang GetBang(int index)
		{
			return Bangs.ElementAtOrDefault(index);
		}

		public Dictionary<string, List<Bang>> GetListOfDicts()
		{
			return DictOfLists;
		}
	}

	public class Bar : IJsonSerialized
    {
		public int NestedProperty { get; set; }

        public override int GetHashCode()
        {
            return NestedProperty.GetHashCode();
        }

        public override bool Equals(object other)
        {
            Bar bar = other as Bar;
            return bar != null && bar.NestedProperty == NestedProperty;
        }
    }

	public class Fizz
	{
		public Fizz()
		{
			Stateful = new Stateful<string>();
		}
		public string Buzz { get; set; }
		public Stateful<string> Stateful { get; set; }
	}

	public class Bang
	{
		public Bang()
		{
			Fizz = new Fizz();
		    SNB = new Stateful<bool?>();
		    FizzyDic = new Dictionary<string, Fizz>();
            Fizzes = new List<Fizz>();
		}
		public int Id { get; set; }
		public Fizz Fizz { get; set; }
        public Stateful<bool?> SNB { get; set; }
        public Dictionary<string, Fizz> FizzyDic { get; set; }

        public List<Fizz> Fizzes { get; set; }

	    public T DeserializeBuzz<T>()
	    {
	        return JsonConvert.DeserializeObject<T>(Fizz.Buzz);
	    }

		public string Data
		{
			get { return ((Id + 1)*2).ToString(); }
		}
	}

	public static class FooExtensions
	{
		public static string Foormat(this DateTime date)
		{
			return date.ToString("yyyy-MM-dd biatch");
		}
		public static string Foormat(this DateTime date, string format)
		{
			return date.ToString(format);
		}

		public static Bar GetBar(this Foo foo)
		{
			return foo.NestedObject;
		}

        public static T DeserializePropA<T>(this Foo foo) where T : class, IJsonSerialized
        {
            return !string.IsNullOrEmpty(foo?.PropA)
                ? JsonConvert.DeserializeObject<T>(foo.PropA)
                : null;
        }
    }
}
