﻿using System;
using JHansen.Utilities.StateManagement;
using Newtonsoft.Json;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace JHansen.Utilities.Tests
{
	[TestFixture]
	public class StatefulTests
	{
		[Test]
		public void StatefulString_String_Operator()
		{
			// Arrange
			Stateful<string> statefulValue = new Stateful<string>("VALPAK");

			// Act
			string value = statefulValue;

			// Assert
			Assert.AreEqual(statefulValue.Value, value);
		}

		[Test]
		public void String_StatefulString_Operator()
		{
			// Arrange
			string value = "VALPAK";

			// Act
			Stateful<string> statefulValue = value;

			// Assert
			Assert.AreEqual(value, statefulValue.Value);
		}

		[Test]
		public void DateTime_StatefulDateTime_ExplicitCast()
		{
			// Arrange
			Stateful<DateTime> statefulValue = new Stateful<DateTime>(DateTime.Now);

			// Act
			DateTime value = (DateTime)statefulValue;

			// Assert
			Assert.AreEqual(statefulValue.Value, value);
		}

		[Test]
		public void DateTime_BoxedStatefulDateTime_ExplicitCast()
		{
			// Arrange
			object statefulValue = new Stateful<DateTime>(DateTime.Now);

			// Act
			object casted = Convert.ChangeType(statefulValue, typeof (DateTime));
            DateTime value = (DateTime)casted;

			// Assert
			Assert.AreEqual(((Stateful<DateTime>)statefulValue).Value, value);
		}

		[Test]
		public void StatefulNullableBool_FromString_true_SetsValue()
		{
			// Arrange
			string value = "true";
			Stateful<bool?> statefulValue = new Stateful<bool?>();

			// Act
			statefulValue.FromString(value);

			// Assert
			Assert.AreEqual(true, statefulValue.Value);
		}

		[Test]
		public void StatefulNullableBool_FromString_false_SetsValueAndState()
		{
			// Arrange
			string value = "false";
			Stateful<bool?> statefulValue = new Stateful<bool?>();

			// Act
			statefulValue.FromString(value);

			// Assert
			Assert.AreEqual(false, statefulValue.Value);
			Assert.AreEqual(ValueState.Visited, statefulValue.State);
		}

		[Test]
		public void StatefulNullableBool_FromString_unknown_SetsState()
		{
			// Arrange
			string value = "unknown";
			Stateful<bool?> statefulValue = new Stateful<bool?>();

			// Act
			statefulValue.FromString(value);

			// Assert
			Assert.AreEqual(ValueState.Unknown, statefulValue.State);
		}

        [Test]
	    public void StatefulString_DeserializeStringValue_RealValue_SetsValueAndState()
	    {
	        // Arrange
	        string json = @"{ ""StatefulString"": { ""stringvalue"": ""realValue"" } }";

	        // Act
	        TestType testType = new TestType();
	        JsonConvert.PopulateObject(json, testType);

            // Assert
	        Assert.AreEqual("realValue", testType.StatefulString.Value);
	        Assert.AreEqual(ValueState.Visited, testType.StatefulString.State);
	    }

	    [Test]
	    public void StatefulString_DeserializeStringValue_unknown_SetsState()
	    {
	        // Arrange
	        string json = @"{ ""StatefulString"": { ""stringvalue"": ""unknown"" } }";

	        // Act
	        TestType testType = new TestType();
	        JsonConvert.PopulateObject(json, testType);

	        // Assert
	        Assert.IsNull(testType.StatefulString.Value);
	        Assert.AreEqual(ValueState.Unknown, testType.StatefulString.State);
	    }

		[Test]
		public void StatefulNullableBool_FromString_unknown_SetsValue()
		{
			// Arrange
			string value = "unknown";
			Stateful<bool?> statefulValue = new Stateful<bool?>(true);

			// Act
			statefulValue.FromString(value);

			// Assert
			Assert.AreEqual(null, statefulValue.Value);
		}

		[Test]
		public void StatefulNullableBool_FromString_none_SetsState()
		{
			// Arrange
			string value = "none";
			Stateful<bool?> statefulValue = new Stateful<bool?>();

			// Act
			statefulValue.FromString(value);

			// Assert
			Assert.AreEqual(ValueState.None, statefulValue.State);
			Assert.AreEqual(null, statefulValue.Value);
		}

		[Test]
		public void StatefulNullableBool_FromString_hidden_SetsState()
		{
			// Arrange
			string value = "_h!dden";
			Stateful<bool?> statefulValue = new Stateful<bool?>(true);

			// Act
			statefulValue.FromString(value);

			// Assert
			Assert.AreEqual(ValueState.Hidden, statefulValue.State);
			Assert.AreEqual(true, statefulValue.Value);
		}

		[Test]
		public void StatefulNullableBool_FromString_removed_SetsState()
		{
			// Arrange
			string value = "_rem0ved";
			Stateful<bool?> statefulValue = new Stateful<bool?>(true);

			// Act
			statefulValue.FromString(value);

			// Assert
			Assert.AreEqual(ValueState.Removed, statefulValue.State);
			Assert.AreEqual(null, statefulValue.Value);
		}

		[Test]
		public void Stateful_ValueChanging_FiresEvent()
		{
			// Arrange
			Stateful<string> stateful = new Stateful<string>();
			bool fired = false;
			stateful.OnValueChanging += (sender, e) =>
			{
				fired = true;
			};

			// Act
			stateful.Value = "New value";

			// Assert
			Assert.IsTrue(fired);
		}

		[Test]
		public void Stateful_ValueChanging_HandlerSetNewValue()
		{
			// Arrange
			Stateful<string> stateful = new Stateful<string>();
			stateful.OnValueChanging += (sender, e) =>
			{
				e.NewValue = "Forcibly Changed";
			};

			// Act
			stateful.Value = "New value";

			// Assert
			Assert.AreEqual("Forcibly Changed", stateful.Value);
		}

		[Test]
		public void Stateful_ValueChanging_HandlerPreventStateChange()
		{
			// Arrange
			Stateful<string> stateful = new Stateful<string>();
			stateful.OnValueChanging += (sender, e) =>
			{
				e.PreventStateChange = true;
			};

			// Act
			stateful.Value = "New value";

			// Assert
			Assert.AreEqual(null, stateful.State);
		}

		[Test]
		public void Stateful_ValueChanging_HandlerDoesNotPreventStateChange()
		{
			// Arrange
			Stateful<string> stateful = new Stateful<string>();
			stateful.OnValueChanging += (sender, e) =>
			{
				e.PreventStateChange = false;
			};

			// Act
			stateful.Value = "New value";

			// Assert
			Assert.AreEqual(ValueState.Visited, stateful.State);
		}

		[Test]
		public void Stateful_NoValueChangingHandler_SetValue()
		{
			// Arrange
			Stateful<string> stateful = new Stateful<string>();

			// Act
			stateful.Value = "New value";

			// Assert
			Assert.AreEqual(ValueState.Visited, stateful.State);
			Assert.AreEqual("New value", stateful.Value);
		}

		[Test]
		public void StatefulDateTime_FromValue_StatefulDateTime_SetsValue()
		{
			// Arrange
			Stateful<string> stateful = new Stateful<string> { Value = "Old Value" };
			Stateful<string> statefulNew = new Stateful<string>{ Value = "New value" };

			// Act
			stateful.FromValue(statefulNew);

			// Assert
			Assert.AreEqual("New value", stateful.Value);
		}

		[Test]
		public void StatefulNullableDateTime_FromValue_DateTime_SetsValue()
		{
			// Arrange
			Stateful<DateTime?> stateful = new Stateful<DateTime?> { Value = DateTime.Now };
			DateTime newValue = DateTime.Now.AddDays(38);

			// Act
			stateful.FromValue(newValue);

			// Assert
			Assert.AreEqual(newValue, stateful.Value);
		}

		[Test]
		public void StatefulNullableBool_FromValue_UnknownSNB_SetsState()
		{
			// Arrange
			Stateful<bool?> stateful = new Stateful<bool?>();
			Stateful<bool?> newValue = new Stateful<bool?> { State = ValueState.Unknown };

			// Act
			stateful.FromValue(newValue);

			// Assert
			Assert.AreEqual(ValueState.Unknown, stateful.State);
		}

		[Test]
		public void StatefulString_FromValue_ValueState_SetsState()
		{
			// Arrange
			Stateful<string> stateful = new Stateful<string>("Val");
			ValueState newState = ValueState.Hidden;

			// Act
			stateful.FromValue(newState);

			// Assert
			Assert.AreEqual(ValueState.Hidden, stateful.State);
			Assert.AreEqual("Val", stateful.Value);
		}

		[Test]
		public void StatefulString_FromValue_NullableValueState_SetsState()
		{
			// Arrange
			Stateful<string> stateful = new Stateful<string>("Val");
			ValueState? newState = ValueState.Hidden;

			// Act
			stateful.FromValue(newState);

			// Assert
			Assert.AreEqual(ValueState.Hidden, stateful.State);
			Assert.AreEqual("Val", stateful.Value);
		}

		[Test]
		public void Stateful_ConvertToValueType_ReturnsValue()
		{
			// Arrange
			Stateful<DateTime> stateful = new Stateful<DateTime> {Value = DateTime.Now};

			// Act
			DateTime value = Convert.ToDateTime(stateful);

			// Assert
			Assert.AreEqual(stateful.Value, value);
		}

		[Test]
		public void Stateful_ConvertChangeTypeToValueType_ReturnsValue()
		{
			// Arrange
			Stateful<DateTime> stateful = new Stateful<DateTime> { Value = DateTime.Now };

			// Act
			object value = Convert.ChangeType(stateful, typeof(DateTime));

			// Assert
			Assert.IsInstanceOf<DateTime>(value);
			Assert.AreEqual(stateful.Value, value);
		}

		[Test]
		public void Stateful_ConvertToDifferentType_ReturnsValueConverted()
		{
			// Arrange
			Stateful<int> stateful = new Stateful<int> {Value = 38};

			// Act
			long value = Convert.ToInt64(stateful);

			// Assert
			Assert.AreEqual(stateful.Value, value);
		}

		[Test]
		public void Stateful_ConvertToUnconvertibleType_Throws()
		{
			// Arrange
			Stateful<bool> stateful = new Stateful<bool> {Value = false};

			// Act
			ActualValueDelegate<DateTime> act = () => Convert.ToDateTime(stateful);

            // Assert -- boom!
            Assert.That(act, Throws.TypeOf<InvalidCastException>());
        }

		[Test]
		public void Stateful_UnknownToString_ReturnsUNKNOWN()
		{
			// Arrange
			Stateful<int> stateful = new Stateful<int> {State = ValueState.Unknown};
			
			// Act
			string value = stateful.ToString();

			// Assert
			Assert.AreEqual("UNKNOWN", value);
		}

		[Test]
		public void Stateful_ConstructorUnknown_SetsValue()
		{
			// Arrange + Act
			Stateful<string> stateful = new Stateful<string>("UNKNOWN");

			// Assert
			Assert.AreEqual(ValueState.Unknown, stateful.State);
			Assert.IsNull(stateful.Value);
		}

		[Test]
		public void Stateful_NoneToString_ReturnsNONE()
		{
			// Arrange
			Stateful<int> stateful = new Stateful<int> { State = ValueState.None };

			// Act
			string value = stateful.ToString();

			// Assert
			Assert.AreEqual("NONE", value);
		}

		[Test]
		public void Stateful_RemovedToString_ReturnsDefaultValue()
		{
			// Arrange
			Stateful<int> stateful = new Stateful<int> (38);
			stateful.State = ValueState.Removed;

			// Act
			string value = stateful.ToString();

			// Assert
			Assert.AreEqual("0", value);
		}

		[Test]
		public void Stateful_HiddenToString_ReturnsValueToString()
		{
			// Arrange
			Stateful<int> stateful = new Stateful<int>(38);
			stateful.State = ValueState.Hidden;

			// Act
			string value = stateful.ToString();

			// Assert
			Assert.AreEqual("38", value);
		}

		[Test]
		public void Stateful_VisitedToString_ReturnsValueToString()
		{
			// Arrange
			Stateful<long> stateful = new Stateful<long> {Value = 38};

			// Act
			string value = stateful.ToString();

			// Assert
			Assert.AreEqual(stateful.Value.ToString(), value);
		}

		[Test]
		public void Stateful_StateNotSetToString_ReturnsValueToString()
		{
			// Arrange
			Stateful<int> stateful = new Stateful<int>();

			// Act
			string value = stateful.ToString();

			// Assert
			Assert.AreEqual(stateful.Value.ToString(), value);
		}

		[Test]
		public void StatefulString_JsonSerialize_WritesValueAndState()
		{
			// Arrange
			Stateful<string> stateful = new Stateful<string>("foo");

			// Act
			string serialized = JsonConvert.SerializeObject(stateful);

			// Assert
			Assert.IsTrue(serialized.Contains("foo"), "serialized stateful doesn't contain the value");
			Assert.IsTrue(serialized.Contains("Visited"), "serialized stateful doesn't contain the state");
		}

		[Test]
		public void StatefulString_JsonSerialize_DeserializeWithSameValueAndState()
		{
			// Arrange
			var stateful = new Stateful<string>("foo");

			// Act
			string serialized = JsonConvert.SerializeObject(stateful);
			var deserialized = JsonConvert.DeserializeObject<Stateful<string>>(serialized);

			// Assert
			Assert.AreEqual(stateful.Value, deserialized.Value);
			Assert.AreEqual(stateful.State, deserialized.State);
		}

		[Test]
		public void StatefulDecimal_JsonSerialize_DeserializeWithSameValueAndState()
		{
			// Arrange
			var stateful = new Stateful<Decimal>(38.32M);

			// Act
			string serialized = JsonConvert.SerializeObject(stateful);
			var deserialized = JsonConvert.DeserializeObject<Stateful<Decimal>>(serialized);

			// Assert
			Assert.AreEqual(stateful.Value, deserialized.Value);
			Assert.AreEqual(stateful.State, deserialized.State);
		}

		[Test]
		public void StatefulNullableBool_JsonSerialize_DeserializeWithSameValueAndState()
		{
			// Arrange
			var stateful = new Stateful<bool?>(true);

			// Act
			string serialized = JsonConvert.SerializeObject(stateful);
			var deserialized = JsonConvert.DeserializeObject<Stateful<bool?>>(serialized);

			// Assert
			Assert.AreEqual(stateful.Value, deserialized.Value);
			Assert.AreEqual(stateful.State, deserialized.State);
		}

		[Test]
		public void StatefulNullableDateTimeUnknown_JsonSerialize_DeserializeWithSameValueAndState()
		{
			// Arrange
			var stateful = new Stateful<DateTime?>
			{
				State = ValueState.Unknown
			};

			// Act
			string serialized = JsonConvert.SerializeObject(stateful);
			var deserialized = JsonConvert.DeserializeObject<Stateful<DateTime?>>(serialized);

			// Assert
			Assert.AreEqual(stateful.Value, deserialized.Value);
			Assert.AreEqual(stateful.State, deserialized.State);
		}

		private class TestStatefulContainer
		{
			public TestStatefulContainer()
			{
				Property = new Stateful<string>();
			}

			public Stateful<string> Property { get; set; }
		}

		[Test]
		public void StatefulString_JsonPopulateObject_DeserializeWithSameValueAndState()
		{
			// Arrange
			var obj = new TestStatefulContainer
			{
				Property = new Stateful<string>
				{
					Value = "charles",
					State = ValueState.Visited
				}
			};

			// Act
			string serialized = JsonConvert.SerializeObject(obj);
			var newInstance = new TestStatefulContainer();
			JsonConvert.PopulateObject(serialized, newInstance);

			// Assert
			Assert.AreEqual(obj.Property.Value, newInstance.Property.Value);
			Assert.AreEqual(obj.Property.State, newInstance.Property.State);
		}
        
	}

    public class TestType
    {
        public TestType()
        {
            StatefulString = new Stateful<string>();
        }

        public Stateful<string> StatefulString { get; set; }
    }
}
