﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace JHansen.Utilities.Tests
{
	[TestFixture]
	public class StringExtensionTests
	{
		[Test]
		public void JoinNonEmpty_OneEmpty_JoinsTheRest()
		{
			// Act
			string result = ",".JoinNonEmpty("blue", "lights", "", "four");

			// Assert
			Assert.AreEqual("blue,lights,four", result);
		}

		[Test]
		public void JoinNonEmpty_OneNull_JoinsTheRest()
		{
			// Act
			string result = ",".JoinNonEmpty("blue", "lights", null, "four");

			// Assert
			Assert.AreEqual("blue,lights,four", result);
		}

		[Test]
		public void JoinNonEmpty_NullsAndEmpties_JoinsTheRest()
		{
			// Act
			string result = ",".JoinNonEmpty("blue", "lights", "", null, "four");

			// Assert
			Assert.AreEqual("blue,lights,four", result);
		}

		[Test]
		public void JoinNonEmpty_AllNullsAndEmpties_JoinsNone()
		{
			// Act
			string result = ",".JoinNonEmpty("", "", "", null, "");

			// Assert
			Assert.AreEqual("", result);
		}

		[Test]
		public void TrimEnd_SourceEndsWithTarget_TargetRemoved()
		{
			// Arrange
			string source = "SomethingLikething";

			// Act
			string result = source.TrimEnd("thing");

			// Assert
			Assert.AreEqual("SomethingLike", result);
		}

		[Test]
		public void TrimEnd_SourceDoesntEndWithTarget_SourceUnchanged()
		{
			// Arrange
			string source = "SomethingLike";

			// Act
			string result = source.TrimEnd("thing");

			// Assert
			Assert.AreEqual(source, result);
		}

		[Test]
		public void TrimEnd_SourceDoesntContainTarget_SourceUnchanged()
		{
			// Arrange
			string source = "SomeLike";

			// Act
			string result = source.TrimEnd("thing");

			// Assert
			Assert.AreEqual(source, result);
		}

		[Test]
		public void TrimEnd_SourceNull_ReturnsNull()
		{
			// Arrange
			string source = null;

			// Act
			string result = source.TrimEnd("thing");

			// Assert
			Assert.IsNull(result);
		}

		[Test]
		public void ToHyphenatedString_SourceNull_ReturnsNull()
		{
			// Arrange
			string source = null;

			// Act
			string result = source.ToHyphenatedString();

			// Assert
			Assert.IsNull(result);
		}

		[Test]
		public void ToHyphenatedString_SourceHasNoHumps_ReturnsSource()
		{
			// Arrange
			string source = "humplessbackwhale";

			// Act
			string result = source.ToHyphenatedString();

			// Assert
			Assert.AreEqual(source, result);
		}

		[Test]
		public void ToHyphenatedString_PascalCaseSource_hyphenates()
		{
			// Arrange
			string source = "humpBackWhale";

			// Act
			string result = source.ToHyphenatedString();

			// Assert
			Assert.AreEqual("hump-back-whale", result);
		}

		[Test]
		public void ToHyphenatedString_CamelCaseSource_hyphenates()
		{
			// Arrange
			string source = "HumpBackWhale";

			// Act
			string result = source.ToHyphenatedString();

			// Assert
			Assert.AreEqual("hump-back-whale", result);
		}

		[Test]
		public void ReplaceTokens_ValidTokens_Replaces()
		{
			// Arrange
			string text = "The {{Fox.Speed}} {{Fox.Color}} fox jumped over the {{jumpee}}";
			Fox fox = new Fox {Speed = "quick", Color = "brown"};
			var dict = new Dictionary<string, string>
			{
				{"jumpee", "moon"}
			};

			// Act
			text = text.ReplaceTokens(fox, dict);

			// Assert
			Assert.AreEqual("The quick brown fox jumped over the moon", text);
		}

		[Test]
		public void ReplaceTokens_NullProperty_ReplacesWithEmpty()
		{
			// Arrange
			string text = "The {{Fox.Speed}} {{Fox.Color}} fox jumped over the {{jumpee}}";
			Fox fox = new Fox { Speed = null, Color = "brown" };
			var dict = new Dictionary<string, string>
			{
				{"jumpee", "moon"}
			};

			// Act
			text = text.ReplaceTokens(fox, dict);

			// Assert
			Assert.AreEqual("The  brown fox jumped over the moon", text);
		}

		[Test]
		public void ReplaceTokens_MissingObject_ReplacesValid()
		{
			// Arrange
			string text = "The {{World.Speed}} {{Fox.Color}} fox jumped over the {{jumpee}}";
			Fox fox = new Fox { Speed = null, Color = "brown" };
			var dict = new Dictionary<string, string>
			{
				{"jumpee", "moon"}
			};

			// Act
			text = text.ReplaceTokens(fox, dict);

			// Assert
			Assert.AreEqual("The {{World.Speed}} brown fox jumped over the moon", text);
		}

		[Test]
		public void ReplaceTokens_BogusObjects_ReplacesValid()
		{
			// Arrange
			string text = "The {{Fox.Speed}} {{Fox.Color}} fox jumped over the {{jumpee}}";
			Fox fox = new Fox { Speed = "slow", Color = "brown" };
			var dict = new Dictionary<string, string>
			{
				{"jumpee", "moon"}
			};

			// Act
			text = text.ReplaceTokens(new object[] {fox, 1, null, ConsoleColor.Black}, dict);

			// Assert
			Assert.AreEqual("The slow brown fox jumped over the moon", text);
		}

		[Test]
		public void ReplaceTokens_ValidTokensMultipleObjects_Replaces()
		{
			// Arrange
			string text = "The {{Car.Make}} ran over the {{Fox.Speed}} {{Fox.Color}} fox {{times}}";
			Fox fox = new Fox { Speed = "quick", Color = "brown" };
			Car car = new Car {Year = 1999, Make = "Mazda", Model = "Miata"};
			var dict = new Dictionary<string, string>
			{
				{"times", "repeatedly"}
			};

			// Act
			text = text.ReplaceTokens(new object[] {fox, car}, dict);

			// Assert
			Assert.AreEqual("The Mazda ran over the quick brown fox repeatedly", text);
		}

		//[Test]
		//TODO: This functionality would make a nice enhancement - when the token is simply the name of a type in the list of objects, .ToString() it
		public void ReplaceTokens_NonDottedObjectToken_ReplacesWithObjectToString()
		{
			// Arrange
			string text = "The {{Car}} ran over the {{Fox.Speed}} {{Fox.Color}} fox {{times}}";
			Fox fox = new Fox { Speed = "quick", Color = "brown" };
			Car car = new Car { Year = 1999, Make = "Mazda", Model = "Miata" };
			var dict = new Dictionary<string, string>
			{
				{"times", "repeatedly"}
			};

			// Act
			text = text.ReplaceTokens(new object[] { fox, car }, dict);

			// Assert
			Assert.AreEqual("The 1999 Mazda Miata ran over the quick brown fox repeatedly", text);
		}

		[Test]
		public void ReplaceTokens_InvalidTokens_ReplacesValids()
		{
			// Arrange
			string text = "The {{Fox.Size}} {{Fox.Color}} fox jumped over the {{jumpee}}";
			Fox fox = new Fox { Speed = "quick", Color = "brown" };
			
			// Act
			text = text.ReplaceTokens(fox);

			// Assert
			Assert.AreEqual("The {{Fox.Size}} brown fox jumped over the {{jumpee}}", text);
		}

		[Test]
		public void ReplaceTokens_InvalidTokenHandler_ReplacesValidsInvokesHandler()
		{
			// Arrange
			string text = "The {{Fox.Size}} {{Fox.Color}} fox jumped over the {{jumpee}}";
			Fox fox = new Fox { Speed = "quick", Color = "brown" };

			// Act
			text = text.ReplaceTokens(fox, invalidTokenHandler: m => $"<font color = red>Parameter {{{ m.Groups["token"].Value }}} undefined</font>");

			// Assert
			Assert.AreEqual("The <font color = red>Parameter {Fox.Size} undefined</font> brown fox jumped over the <font color = red>Parameter {jumpee} undefined</font>", text);
		}

		private class Fox
		{
			public string Color { get; set; }
			public string Speed { get; set; }
		}

		private class Car
		{
			public int Year { get; set; }
			public string Make { get; set; }
			public string Model { get; set; }

			public override string ToString()
			{
				return $"{Year} {Make} {Model}";
			}
		}
	}
}
