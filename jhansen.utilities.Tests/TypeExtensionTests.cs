﻿using System;
using JHansen.Utilities.StateManagement;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using System.Linq;

namespace JHansen.Utilities.Tests
{
	[TestFixture]
	public class TypeExtensionTests
	{
		[Test]
		public void GetDefaultValue_String_ReturnsEmpty()
		{
			// Arrange
			Type stringType = typeof (string);

			// Act
			object result = stringType.GetDefaultValue();

			// Assert
			Assert.IsInstanceOf(stringType, result);
			Assert.AreEqual("", result);
		}

		[Test]
		public void GetDefaultValue_Int_ReturnsZero()
		{
			// Arrange
			Type type = typeof(int);

			// Act
			object result = type.GetDefaultValue();

			// Assert
			Assert.IsInstanceOf(type, result);
			Assert.AreEqual(0, result);
		}

		[Test]
		public void GetDefaultValue_Float_ReturnsZero()
		{
			// Arrange
			Type type = typeof(float);

			// Act
			object result = type.GetDefaultValue();

			// Assert
			Assert.IsInstanceOf(type, result);
			Assert.AreEqual(0F, result);
		}

		[Test]
		public void GetDefaultValue_Char_ReturnsEmpty()
		{
			// Arrange
			Type type = typeof(char);

			// Act
			object result = type.GetDefaultValue();

			// Assert
			Assert.IsInstanceOf(type, result);
			Assert.AreEqual(new char(), result);
		}

		[Test]
		public void GetDefaultValue_Bool_ReturnsFalse()
		{
			// Arrange
			Type type = typeof(bool);

			// Act
			object result = type.GetDefaultValue();

			// Assert
			Assert.IsInstanceOf(type, result);
			Assert.AreEqual(false, result);
		}

		[Test]
		public void GetDefaultValue_DateTime_ReturnsMinDate()
		{
			// Arrange
			Type type = typeof(DateTime);

			// Act
			object result = type.GetDefaultValue();

			// Assert
			Assert.IsInstanceOf(type, result);
			Assert.AreEqual(DateTime.MinValue, result);
		}

		public enum TestEnum
		{
			Unknown = 0,
			True,
			False
		}

		[Test]
		public void GetDefaultValue_Enum_Returns0()
		{
			// Arrange
			Type type = typeof(TestEnum);

			// Act
			object result = type.GetDefaultValue();

			// Assert
			Assert.IsInstanceOf(type, result);
			Assert.AreEqual(TestEnum.Unknown, result);
		}

		[Test]
		public void ConvertTo_StatefulString_Unknown_ReturnsCorrectState()
		{
			// Arrange
			Type target = typeof (Stateful<string>);
			string value = "unknown";

			// Act
			object result = value.ConvertTo(target);

			// Assert
			Assert.IsInstanceOf(target, result);
			Stateful<string> castedResult = (Stateful<string>) result;
			Assert.AreEqual(ValueState.Unknown, castedResult.State);
		}

		[Test]
		public void ConvertTo_StatefulInt_Unknown_ReturnsCorrectState()
		{
			// Arrange
			Type target = typeof(Stateful<int>);
			string value = "unknown";

			// Act
			object result = value.ConvertTo(target);

			// Assert
			Assert.IsInstanceOf(target, result);
			Stateful<int> castedResult = (Stateful<int>)result;
			Assert.AreEqual(ValueState.Unknown, castedResult.State);
		}

		[Test]
		public void ConvertTo_StatefulInt_NumericString_ParsesValue()
		{
			// Arrange
			Type target = typeof(Stateful<int>);
			string value = "38";

			// Act
			object result = value.ConvertTo(target);

			// Assert
			Assert.IsInstanceOf(target, result);
			Stateful<int> castedResult = (Stateful<int>)result;
			Assert.AreEqual(ValueState.Visited, castedResult.State);
			Assert.AreEqual(38, castedResult.Value);
		}

		[Test]
		public void ConvertTo_StatefulInt_IntValue_ReturnsValue()
		{
			// Arrange
			Type target = typeof(Stateful<int>);
			int value = 38;

			// Act
			object result = value.ConvertTo(target);

			// Assert
			Assert.IsInstanceOf(target, result);
			Stateful<int> castedResult = (Stateful<int>)result;
			Assert.AreEqual(ValueState.Visited, castedResult.State);
			Assert.AreEqual(value, castedResult.Value);
		}

		[Test]
		public void ConvertTo_StatefulInt_NonIntValue_Throws()
		{
			// Arrange
			Type target = typeof(Stateful<int>);
			Decimal value = 38M;

            // Act
            ActualValueDelegate<object> act = () => value.ConvertTo(target);
            
            // boom #dropthemic
            Assert.That(act, Throws.TypeOf<InvalidCastException>());
        }

		[Test]
		public void ConvertTo_DateTime_EmptyString_ReturnsDefaultDate()
		{
			// Arrange
			string valueToConvert = "";

			// Act
			object converted = valueToConvert.ConvertTo(typeof(DateTime));

			// Assert
			Assert.AreEqual(new DateTime(), converted);
		}

		[Test]
		public void ConvertTo_Enum_InvalidValue_ReturnsEnumDefault()
		{
			// Arrange
			string valueToConvert = "FUBAR";

			// Act
			object converted = valueToConvert.ConvertTo(typeof(MySuperAwesomeEnum));

			// Assert
			Assert.AreEqual(MySuperAwesomeEnum.None, converted);
		}

		[Test]
		public void ConvertTo_Enum_ValidValue_ReturnsEnumValue()
		{
			// Arrange
			string valueToConvert = "Value";

			// Act
			object converted = valueToConvert.ConvertTo(typeof(MySuperAwesomeEnum));

			// Assert
			Assert.AreEqual(MySuperAwesomeEnum.Value, converted);
		}

        [Test]
        public void ConvertTo_NullableGuid_EmptyString_ReturnsNull()
        {
            // Arrange
            string valueToConvert = "";

            // Act
            object converted = valueToConvert.ConvertTo(typeof(Guid?));

            // Assert
            Assert.AreEqual(null, converted);
        }

        [Test]
        public void ConvertTo_NullableGuid_ValidGuid_ReturnsParsed()
        {
            // Arrange
            string valueToConvert = "68002f2d-95e3-4315-8cde-ec47044ab702";

            // Act
            object converted = valueToConvert.ConvertTo(typeof(Guid?));

            // Assert
            Assert.AreEqual("68002f2d-95e3-4315-8cde-ec47044ab702", converted.ToString());
        }

        [Test]
        public void ConvertTo_NullableGuid_InvalidGuid_ThrowsFormatEx()
        {
            // Arrange
            string valueToConvert = "68002f2d-YOLO-ec47044ab702";

            // Act
            ActualValueDelegate<object> act = () => valueToConvert.ConvertTo(typeof(Guid?));

            // Assert -- BOOM
            Assert.That(act, Throws.TypeOf<FormatException>());
        }

        [Test]
		public void GetDefaultValue_String_AlwaysEmpty()
		{
			// Arrange 
			object defaultString1 = null;
			object defaultString2 = null;

			// Act
			defaultString1 = typeof (string).GetDefaultValue();
			defaultString1 = "nondefault";
			defaultString2 = typeof (string).GetDefaultValue();

			// Assert
			Assert.AreEqual(string.Empty, defaultString2);
		}

		[Test]
		public void GetDefaultValue_Int_Always0()
		{
			// Arrange
			object defaultInt1 = null;
			object defaultInt2 = null;

			// Act
			defaultInt1 = typeof (int).GetDefaultValue();
			defaultInt1 = 39;
			defaultInt2 = typeof (int).GetDefaultValue();

			// Assert
			Assert.AreEqual(0, defaultInt2);
		}

		[Test]
		public void IsNumeric_BoxedDouble_True()
		{
			// Arrange
			object boxed = 4D;

			// Act
			bool isNumeric = boxed.GetType().IsNumeric();

			// Assert
			Assert.IsTrue(isNumeric);
		}

		[Test]
		public void IsNumeric_Double_True()
		{
			// Arrange
			double num = 4D;

			// Act
			bool isNumeric = num.GetType().IsNumeric();

			// Assert
			Assert.IsTrue(isNumeric);
		}

		[Test]
		public void IsNumeric_BoxedLong_True()
		{
			// Arrange
			object boxed = 4L;

			// Act
			bool isNumeric = boxed.GetType().IsNumeric();

			// Assert
			Assert.IsTrue(isNumeric);
		}

		[Test]
		public void IsNumeric_Long_True()
		{
			// Arrange
			long num = 4L;

			// Act
			bool isNumeric = num.GetType().IsNumeric();

			// Assert
			Assert.IsTrue(isNumeric);
		}

		[Test]
		public void IsNumeric_UShort_True()
		{
			// Arrange
			ushort num = 4;

			// Act
			bool isNumeric = num.GetType().IsNumeric();

			// Assert
			Assert.IsTrue(isNumeric);
		}

		[Test]
		public void IsNumeric_UInt_True()
		{
			// Arrange
			uint num = 4U;

			// Act
			bool isNumeric = num.GetType().IsNumeric();

			// Assert
			Assert.IsTrue(isNumeric);
		}

		[Test]
		public void IsNumeric_ULong_True()
		{
			// Arrange
			ulong num = 4UL;

			// Act
			bool isNumeric = num.GetType().IsNumeric();

			// Assert
			Assert.IsTrue(isNumeric);
		}

		[Test]
		public void IsNumeric_BoxedString_False()
		{
			// Arrange
			object boxed = "blah";

			// Act
			bool isNumeric = boxed.GetType().IsNumeric();

			// Assert
			Assert.IsFalse(isNumeric);
		}

		[Test]
		public void IsNumeric_String_False()
		{
			// Arrange
			string str = "blah";

			// Act
			bool isNumeric = str.GetType().IsNumeric();

			// Assert
			Assert.IsFalse(isNumeric);
		}
		
        private class TestDefaultClass
		{
			public string SomeProp { get; set; }
		}

		[Test]
		public void GetDefaultValue_Class_AlwaysNew()
		{
			// Arrange
			object instance1 = null;
			object instance2 = null;

			// Act
			instance1 = typeof (TestDefaultClass).GetDefaultValue();
			((TestDefaultClass) instance1).SomeProp = "A non-default value";
			instance2 = typeof (TestDefaultClass).GetDefaultValue();

			// Assert
			Assert.AreNotEqual(((TestDefaultClass) instance1).SomeProp, ((TestDefaultClass) instance2).SomeProp);
			Assert.IsNull(((TestDefaultClass) instance2).SomeProp);
		}

	    [Test]
	    public void ScanAssembliesForMatchingTypes_FindsMatch()
	    {
	        // Arrange
	        MethodInfo method = typeof(RockLobster).GetMethod("DoIt");
	        TypeInfo genericType = (TypeInfo) method.GetGenericArguments().First();

	        // Act
	        List<Type> result = genericType.ScanAssembliesForMatchingTypes("Hoop").ToList();

	        // Assert
	        Assert.AreEqual(1, result.Count);
	        Assert.AreEqual(typeof(Hoop), result.First());
	    }

        [Test]
        public void ScanAssembliesForMatchingTypes_NoConstraints_FindsMatch()
        {
            // Arrange
            MethodInfo method = typeof(RockLobster).GetMethod("Rando");
            TypeInfo genericType = (TypeInfo)method.GetGenericArguments().First();

            // Act
            List<Type> result = genericType.ScanAssembliesForMatchingTypes("Hoop").ToList();

            // Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(typeof(Hoop), result.First());
        }

        [Test]
        public void ScanAssembliesForMatchingTypes_PartialNamespace_FindsMatch()
        {
            // Arrange
            MethodInfo method = typeof(RockLobster).GetMethod("DoIt");
            TypeInfo genericType = (TypeInfo)method.GetGenericArguments().First();

            // Act
            List<Type> result = genericType.ScanAssembliesForMatchingTypes("Tests.TypeExtensionTests+Hoop").ToList();

            // Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(typeof(Hoop), result.First());
        }

        [Test]
        public void ScanAssembliesForMatchingTypes_PartialClassName_DoesntFindMatch()
        {
            // Arrange
            MethodInfo method = typeof(RockLobster).GetMethod("DoIt");
            TypeInfo genericType = (TypeInfo)method.GetGenericArguments().First();

            // Act
            List<Type> result = genericType.ScanAssembliesForMatchingTypes("oop").ToList();

            // Assert
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void ScanAssembliesForMatchingTypes_WrongName_FindsNoMatch()
        {
            // Arrange
            MethodInfo method = typeof(RockLobster).GetMethod("DoIt");
            TypeInfo genericType = (TypeInfo)method.GetGenericArguments().First();

            // Act
            List<Type> result = genericType.ScanAssembliesForMatchingTypes("Hoopla").ToList();

            // Assert
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void MethodInfoMakeGenericMethod_ValidTypes_ReturnsGenericMethod()
        {
            // Arrange
            MethodInfo method = typeof(RockLobster).GetMethod("DoIt");

            // Act
            MethodInfo result = method.MakeGenericMethod("Hula");

            // Assert
            Assert.IsTrue(result.IsGenericMethod);
            Assert.IsFalse(result.IsGenericMethodDefinition);
            Assert.AreEqual(typeof(Hula), result.GetGenericArguments().First());
        }

        [Test]
	    public void TypeIsReallyAssignableFrom_ValidTypeParameter_ReturnsTrue()
	    {
	        // Arrange
	        var method = typeof(TestTypeExtensions).GetMethod("GenericMethod");
            Type type0 = method.GetParameters()[0].ParameterType;

            // Act
	        bool result = type0.IsReallyAssignableFrom(typeof(DateTime));

	        // Assert
	        Assert.IsTrue(result);
	    }

	    [Test]
	    public void TypeIsReallyAssignableFrom_ValidGenericType_ReturnsTrue()
	    {
	        // Arrange
	        var method = typeof(TestTypeExtensions).GetMethod("GenericMethod");
	        Type type1 = method.GetParameters()[1].ParameterType;

	        // Act
	        bool result = type1.IsReallyAssignableFrom(typeof(List<CandidateD>));

	        // Assert
	        Assert.IsTrue(result);
	    }

	    [Test]
	    public void TypeIsReallyAssignableFrom_InvalidGenericTypePrivateConstructor_ReturnsFalse()
	    {
	        // Arrange
	        var method = typeof(TestTypeExtensions).GetMethod("GenericMethod");
	        Type type1 = method.GetParameters()[1].ParameterType;

	        // Act
	        bool result = type1.IsReallyAssignableFrom(typeof(List<CandidateA>));

	        // Assert
	        Assert.IsFalse(result);
	    }

	    [Test]
	    public void TypeIsReallyAssignableFrom_InvalidGenericTypeStruct_ReturnsFalse()
	    {
	        // Arrange
	        var method = typeof(TestTypeExtensions).GetMethod("GenericMethod");
	        Type type1 = method.GetParameters()[1].ParameterType;

	        // Act
	        bool result = type1.IsReallyAssignableFrom(typeof(List<CandidateB>));

	        // Assert
	        Assert.IsFalse(result);
	    }

	    [Test]
	    public void TypeIsReallyAssignableFrom_InvalidGenericTypeNoInterface_ReturnsFalse()
	    {
	        // Arrange
	        var method = typeof(TestTypeExtensions).GetMethod("GenericMethod");
	        Type type1 = method.GetParameters()[1].ParameterType;

	        // Act
	        bool result = type1.IsReallyAssignableFrom(typeof(List<CandidateC>));

	        // Assert
	        Assert.IsFalse(result);
	    }

        public class Hula
	    {
	        
	    }

        public class Hoop : Hula
	    {
	        
	    }

        public class RockLobster
	    {
	        public void DoIt<T>(T stuff) where T : Hula
	        {
	            
	        }

            public void Rando<T>(T stuff) 
            {

            }
        }

	    public static class TestTypeExtensions
	    {
	        public static void GenericMethod<T0, T1>(T0 direct, IEnumerable<T1> generic)
	            where T0 : struct
	            where T1 : class, IInterface, new()
            { }
        }

	    public interface IInterface { }

	    public class CandidateA : IInterface
	    {
	        private CandidateA()
	        {
	        }
	    }
	    public struct CandidateB : IInterface { }

	    public class CandidateC
	    {
	        public CandidateC()
	        {
	        }
	    }

	    public class CandidateD : IInterface
	    {
	        public CandidateD()
	        {
	        }
	    }
    }

	internal enum MySuperAwesomeEnum
	{
		None = 0,
		Value,
		OtherValue
	}
}