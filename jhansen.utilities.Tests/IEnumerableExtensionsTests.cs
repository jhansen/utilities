﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace JHansen.Utilities.Tests
{
    [TestFixture]
    public class IEnumerableExtensionsTests
    {
        [Test]
        public void GetPropertyValue_FakePropertyName_ReturnsNulls()
        {
            // arrange
            var instance = new List<Foo> { new Foo { PropA = "bar" } };
            string prop = "Foo.IDontExist";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.IsNull(value);
        }

        [Test]
        public void GetPropertyValue_FakeClassName_ReturnsNulls()
        {
            // arrange
            var instance = new List<Foo> { new Foo { PropA = "bar" } };
            string prop = "IDontExist.Blah";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.IsNull(value);
        }

        [Test]
        public void GetPropertyValue_TopLevelProperty_GetsValue()
        {
            // arrange
            var instance = new List<Foo> { new Foo { PropA = "bar" } };
            string prop = "Foo.PropA";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.AreEqual(instance[0].PropA, value);
        }

        [Test]
        public void GetPropertyValue_NestedObject_GetsValue()
        {
            // arrange
            var instance = new List<Foo> { new Foo() };
            string prop = "Foo.NestedObject";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.AreEqual(instance[0].NestedObject, value);
            Assert.AreEqual(instance[0].NestedObject.NestedProperty, ((Bar) value).NestedProperty);
        }

		[Test]
		public void GetPropertyValue_MethodWithBopnArgument_GetsValue()
		{
			// arrange
			var instance = new List<Foo> { new Foo { PropA = "Text" } };
			string prop = "Foo.Reverse(~Foo.PropA)";

			// act
			var value = instance.GetPropertyValue(prop);

			// assert
			Assert.AreEqual("txeT", value);
		}

	    [Test]
	    public void GetPropertyValue_DynamicLinqWithNestedBopn_GetsValue()
	    {
		    // arrange
		    var objects = new List<object> { new Foo { Bangs = { new Bang { Id  = 1 }, new Bang { Id = 2 } } }, new Bar{ NestedProperty = 2 } };
		    string prop = "Foo.Bangs{Id == ~Bar.NestedProperty}.Id";

		    // act
		    var value = objects.GetPropertyValue(prop);

		    // assert
		    Assert.AreEqual(2, value);
	    }

        [Test]
        public void GetPropertyValue_DynamicLinqWithDoubleNestedBopn_GetsValue()
        {
            // arrange
            var objects = new List<object> { new Foo { Bangs = { new Bang { Id = 1 }, new Bang { Id = 2 } } }, new Bang { Fizzes = { new Fizz{ Buzz = "2" } } } };
            string prop = "Foo.Bangs{Id.ToString() == \"~(Bang.Fizzes{Buzz == \"~(Foo.Bangs.Last<Bang>().Id)\" }.Buzz)\"}.Id";

            // act
            var value = objects.GetPropertyValue(prop);

            // assert
            Assert.AreEqual(2, value);
        }

        [Test]
	    public void GetPropertyValue_DynamicLinqWithNestedBopnAsString_GetsValue()
	    {
		    // arrange
		    var objects = new List<object> { new Foo { Bangs = { new Bang { Id = 1 }, new Bang { Id = 2 } } }, new Bar { NestedProperty = 2 } };
		    string prop = "Foo.Bangs{Id.ToString() == \"~Bar.NestedProperty\"}.Id";

		    // act
		    var value = objects.GetPropertyValue(prop);

		    // assert
		    Assert.AreEqual(2, value);
	    }

		[Test]
		public void GetPropertyValue_MethodWithBopnArgumentEmptyValue_GetsValue()
		{
			// arrange
			var instance = new List<Foo> { new Foo { PropA = "" } };
			string prop = "Foo.Reverse(~Foo.PropA)";

			// act
			var value = instance.GetPropertyValue(prop);

			// assert
			Assert.AreEqual("", value);
		}

		[Test]
		public void GetPropertyValue_MethodWithBopnArgumentNullValue_GetsValue()
		{
			// arrange
			var instance = new List<Foo> { new Foo { PropA = null } };
			string prop = "Foo.Reverse(~Foo.PropA)";

			// act
			var value = instance.GetPropertyValue(prop);

			// assert
			Assert.AreEqual("", value);
		}

		[Test]
        public void GetPropertyValue_NestedObjectProperty_GetsValue()
        {
            // arrange
            var instance = new List<Foo> { new Foo() };
            string prop = "Foo.NestedObject.NestedProperty";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.AreEqual(instance[0].NestedObject.NestedProperty, value);
        }

        [Test]
        public void GetPropertyValue_FakePropertyName_Multiple_ReturnsNulls()
        {
            // arrange
            var instance = new List<object> { new Foo { PropA = "bar" }, new Bar { NestedProperty = 8 } };
            string prop = "Foo.IDontExist";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.IsNull(value);
        }

        [Test]
        public void GetPropertyValue_FakeClassName_Multiple_ReturnsNulls()
        {
            // arrange
            var instance = new List<object> { new Foo { PropA = "bar" }, new Bar { NestedProperty = 8 } };
            string prop = "IDontExist.Blah";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.IsNull(value);
        }

        [Test]
        public void GetPropertyValue_TopLevelProperty_Multiple_GetsValues()
        {
            // arrange
            var instance = new List<object> { new Foo { PropA = "bar" }, new Bar { NestedProperty = 8 } };
            string prop1 = "Foo.PropA";
            string prop2 = "Bar.NestedProperty";

            // act
            var value1 = instance.GetPropertyValue(prop1);
            var value2 = instance.GetPropertyValue(prop2);

            // assert
            Assert.AreEqual(((Foo)instance[0]).PropA, value1);
            Assert.AreEqual(((Bar)instance[1]).NestedProperty, value2);
        }

        [Test]
        public void GetPropertyValue_NestedObject_Multiple_GetsValue()
        {
            // arrange
            var instance = new List<object> { new Foo(), new Bar() };
            string prop = "Foo.NestedObject";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.AreEqual(((Foo)instance[0]).NestedObject, value);
            Assert.AreEqual(((Foo)instance[0]).NestedObject.NestedProperty, ((Bar)value).NestedProperty);
        }

        [Test]
        public void GetPropertyValue_NestedObjectProperty_Multiple_GetsValue()
        {
            // arrange
            var instance = new List<object> { new Foo(), new Bar() };
            string prop = "Foo.NestedObject.NestedProperty";

            // act
            var value = instance.GetPropertyValue(prop);

            // assert
            Assert.AreEqual(((Foo)instance[0]).NestedObject.NestedProperty, value);
        }

		[Test]
		public void GetPropertyEvalInfo_NestedObjectProperty_Multiple_GetsInfo()
		{
			// arrange
			var instance = new List<object> { new Foo(), new Bar() };
			string prop = "Foo.NestedObject.NestedProperty";

			// act
			var info = instance.GetPropertyEvalInfo(prop);

			// assert
			Assert.IsTrue(info.SuccessfullyEvaluated);
			Assert.AreEqual(((Foo)instance[0]).NestedObject.NestedProperty, info.GetValue());
		}

		[Test]
	    public void Any_NullablePredicate_IgnoresNull()
	    {
		    // Arrange
			Foo foo = GetNullablePredicateTestObject();

			// Act
			bool any2 = foo.Bangs.Any(b => b.Fizz?.Buzz == "2");
			bool any3 = foo.Bangs.Any(b => b.Fizz?.Buzz == "3");

		    // Assert
		    Assert.IsTrue(any2);
		    Assert.IsFalse(any3);
	    }

		[Test]
		public void All_NullablePredicate_IgnoresNull()
		{
			// Arrange
			Foo foo = GetNullablePredicateTestObject();

			// Act
			bool all2 = foo.Bangs.All(b => b.Fizz?.Buzz == "2");
			bool allNot3 = foo.Bangs.All(b => b.Fizz?.Buzz != "3");

			// Assert
			Assert.IsFalse(all2);
			Assert.IsTrue(allNot3);
		}

		[Test]
		public void Where_NullablePredicate_IgnoresNull()
		{
			// Arrange
			var foo = GetNullablePredicateTestObject();

			// Act
			var where2 = foo.Bangs.Where(b => b.Fizz?.Buzz == "2");
			var where3 = foo.Bangs.Where(b => b.Fizz?.Buzz == "3");

			// Assert
			Assert.AreEqual(1, where2.Count());
			Assert.AreEqual(0, where3.Count());
		}

		[Test]
		public void First_NullablePredicate_IgnoresNull()
		{
			// Arrange
			var foo = GetNullablePredicateTestObject();

			// Act
			var first2 = foo.Bangs.First(b => b.Fizz?.Buzz == "2");

			// Assert
			Assert.AreSame(foo.Bangs.First(b => b.Id == 2), first2);
		}

		[Test]
		public void FirstOrDefault_NullablePredicate_IgnoresNull()
		{
			// Arrange
			var foo = GetNullablePredicateTestObject();

			// Act
			var first2 = foo.Bangs.FirstOrDefault(b => b.Fizz?.Buzz == "2");
			var first3 = foo.Bangs.FirstOrDefault(b => b.Fizz?.Buzz == "3");

			// Assert
			Assert.AreSame(foo.Bangs.FirstOrDefault(b => b.Id == 2), first2);
			Assert.IsNull(first3);
		}

		[Test]
		public void Last_NullablePredicate_MatchesNull()
		{
			// Arrange
			var foo = GetNullablePredicateTestObject();

			// Act
			var lastNot2 = foo.Bangs.Last(b => b.Fizz?.Buzz != "2");
			var lastNullBuzz = foo.Bangs.Last(b => b.Fizz?.Buzz == null);

			// Assert
			Assert.AreSame(foo.Bangs.First(b => b.Id == 3), lastNot2);
			Assert.AreSame(foo.Bangs.First(b => b.Id == 3), lastNullBuzz);
		}

		[Test]
		public void LastOrDefault_NullablePredicate_MatchesNull()
		{
			// Arrange
			var foo = GetNullablePredicateTestObject();

			// Act
			var lastNot2 = foo.Bangs.LastOrDefault(b => b.Fizz?.Buzz != "2");
			var lastNullBuzz = foo.Bangs.LastOrDefault(b => b.Fizz?.Buzz == null);

			// Assert
			Assert.AreSame(foo.Bangs.First(b => b.Id == 3), lastNot2);
			Assert.AreSame(foo.Bangs.First(b => b.Id == 3), lastNullBuzz);
		}

		[Test]
		public void LastOrDefault_NullablePredicate_IgnoresNull()
		{
			// Arrange
			var foo = GetNullablePredicateTestObject();

			// Act
			var last3 = foo.Bangs.LastOrDefault(b => b.Fizz?.Buzz == "3");

			// Assert
			Assert.IsNull(last3);
		}

		private Foo GetNullablePredicateTestObject()
	    {
		    return new Foo
			{
				Bangs =
				{
					new Bang {Id = 1, Fizz = { Buzz = "1" }},
					new Bang {Id = 2, Fizz = { Buzz = "2" }},
					new Bang {Id = 3, Fizz = null}
				}
			};
		}
	}
}
