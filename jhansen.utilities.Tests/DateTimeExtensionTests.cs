﻿using System;
using System.Globalization;
using NUnit.Framework;

namespace JHansen.Utilities.Tests
{
	/// <summary>
	/// Summary description for DateTimeExtensionTests
	/// </summary>
	[TestFixture]
	public class DateTimeExtensionTests
	{
		[Test]
		public void ToUniversalTime_UnspecifiedKindDefaultUtc_SetsKind()
		{
			// Arrange
			DateTime value = DateTime.SpecifyKind(
				TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.ParseExact("01/01/2001 14:00:00", "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal),
					"Pacific Standard Time"),
				DateTimeKind.Unspecified);

			// Act
			DateTime universalized = value.ToUniversalTime(DateTimeKind.Utc);

			// Assert
			Assert.AreEqual(DateTimeKind.Unspecified, value.Kind);
			Assert.AreEqual(DateTimeKind.Utc, universalized.Kind);
			Assert.AreEqual(value, universalized);
		}

		[Test]
		public void ToUniversalTime_null_ReturnsNull()
		{
			// Arrange
			DateTime? value = null;

			// Act
			DateTime? universalized = value.ToUniversalTime(DateTimeKind.Utc);

			// Assert
			Assert.IsNull(universalized);
		}

		[Test]
		public void ToUniversalTime_NonNull_ReturnsValueToUtc()
		{
			// Arrange
			DateTime? value = DateTime.SpecifyKind(
				TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.ParseExact("01/01/2001 14:00:00", "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal),
					"Pacific Standard Time"),
				DateTimeKind.Unspecified);

			// Act
			DateTime? universalized = value.ToUniversalTime(DateTimeKind.Utc);

			// Assert
			Assert.AreEqual(DateTimeKind.Unspecified, value.Value.Kind);
			Assert.AreEqual(DateTimeKind.Utc, universalized.Value.Kind);
			Assert.AreEqual(value, universalized);
		}

		[Test]
		public void ToUniversalTime_LocalKindDefaultUtc_SetsKindAndConverts()
		{
			// Arrange
			DateTime value = DateTime.SpecifyKind(
				TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.ParseExact("01/01/2001 14:00:00", "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal), 
					"Pacific Standard Time"),
				DateTimeKind.Local);

			// Act
			DateTime universalized = value.ToUniversalTime(DateTimeKind.Utc);

			// Assert
			Assert.AreEqual(DateTimeKind.Local, value.Kind);
			Assert.AreEqual(DateTimeKind.Utc, universalized.Kind);
			Assert.AreEqual(value.ToUniversalTime(), universalized); // check it against the .NET ToUniversalTime() result
		}

		[Test]
		public void DateTime_ConvertFromUtc_ToMountainTime_Converts()
		{
			// Arrange
			DateTime value = DateTime.SpecifyKind(
				TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.ParseExact("01/01/2001 14:00:00", "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal),
					"Utc"),
				DateTimeKind.Utc);
			TimeZoneInfo mountainous = TimeZoneInfo.FindSystemTimeZoneById("US Mountain Standard Time");

			// Act
			DateTime local = value.ConvertFromUtc(mountainous);

			// Assert
			Assert.AreEqual(DateTimeKind.Local, local.Kind);
			Assert.AreEqual(DateTimeKind.Utc, value.Kind);
			// TimeZoneInfo.ConvertTimeToUtc throws an error when DateTimeKind is local and the specified timezone isn't the local timezone
			DateTime unspecifiedMountain = DateTime.SpecifyKind(local, DateTimeKind.Unspecified);
            Assert.AreEqual(TimeZoneInfo.ConvertTimeToUtc(unspecifiedMountain, mountainous), value); // check it against the .NET ConvertTimeToUtc() result
		}

		[Test]
		public void TimeSpan_ConvertFromUtc_ToMountainTime_Converts()
		{
			// Arrange
			TimeSpan value = new TimeSpan(14, 0, 0);
			TimeZoneInfo mountainous = TimeZoneInfo.FindSystemTimeZoneById("US Mountain Standard Time");

			// Act
			TimeSpan mountainousTime = value.ConvertFromUtc(mountainous, new DateTime(2001, 1, 1));

			// Assert
			DateTime utcDateTime = DateTime.SpecifyKind(
				TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.ParseExact("01/01/2001 14:00:00", "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal),
					"Utc"),
				DateTimeKind.Utc);
			// use the baked-in .NET DateTime conversion to check our result
			DateTime mountainousDateTime = TimeZoneInfo.ConvertTime(utcDateTime, mountainous);
			Assert.AreEqual(mountainousDateTime.TimeOfDay, mountainousTime);
		}

		[Test]
		public void TimeSpan_ToUniversalTime_FromMountainTime_Converts()
		{
			// Arrange
			TimeSpan mountainTime = new TimeSpan(14, 0, 0);
			TimeZoneInfo mountainous = TimeZoneInfo.FindSystemTimeZoneById("US Mountain Standard Time");

			// Act
			TimeSpan utcTime = mountainTime.ToUniversalTime(mountainous, new DateTime(2001, 1, 1));

			// Assert
			DateTime utcDateTime = new DateTime(new DateTime(2001, 1, 1).Ticks + utcTime.Ticks, DateTimeKind.Utc);
			// use the baked-in .NET DateTime conversion to check our result
			DateTime mountainDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, mountainous);
			Assert.AreEqual(mountainDateTime.TimeOfDay, mountainTime);
		}
	}
}
