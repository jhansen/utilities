﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace JHansen.Utilities
{
	public static partial class ExtensionMethods
	{
		/// <summary>
		/// Using reflection, attempts to find a method matching the MethodName and ParentType of the given MethodSignatureKey
		/// and the given string values for arguments. If no matching method is found on the ParentType, all assemblies loaded
		/// in the AppDomain will be scanned for a matching extension method.
		/// </summary>
		public static MethodInfo FindMethod(this MethodSignatureKey key, string[] argValues)
		{
            MethodInfo result = key.ParentType.GetMethods()
				.FirstOrDefault(method => method.IsMatch(key, argValues))
				?? AppDomain.CurrentDomain.GetExportedTypes() // no method found on the target type, check for extension methods
					.Where(type => type.IsSealed && !type.IsGenericType && !type.IsNested)
					.SelectMany(t => t.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
					.FirstOrDefault(method => method.IsDefined(typeof(ExtensionAttribute), false)
						&& method.IsMatch(key, argValues));

		    return result?.IsGenericMethod == true
		        ? result.MakeGenericMethod(key.GenericTypes)
		        : result;
		}
    }
}
