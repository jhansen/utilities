﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace JHansen.Utilities
{
	public static partial class ExtensionMethods
	{
		/// <summary>
		/// Converts the XDocument into an XML document.
		/// </summary>
		/// <param name="xDocument">The XDocument to be converted.</param>
		/// <returns>The Xml document version of the XDocument.</returns>
		public static XmlDocument ToXmlDocument(this XDocument xDocument)
		{
			var xmlDocument = new XmlDocument();
			using (var xmlReader = xDocument.CreateReader())
			{
				xmlDocument.Load(xmlReader);
			}
			return xmlDocument;
		}

		/// <summary>
		/// Converts an Xml node into an XDocument.
		/// </summary>
		/// <param name="node">The Xml node to be converted.</param>
		/// <returns>The XDocument version of the Xml node.</returns>
		public static XDocument ToXDocument(this XmlNode node)
		{
			using (var nodeReader = new XmlNodeReader(node))
			{
				nodeReader.MoveToContent();
				return XDocument.Load(nodeReader);
			}
		}

		/// <summary>
		/// Gets the specified element from a document.
		/// </summary>
		/// <param name="xDoc">The document to search.</param>
		/// <param name="name">The name of the element to find.</param>
		/// <returns>The specified element from the document.</returns>
		public static XElement GetElement(this XDocument xDoc, string name)
		{
			return GetElement(xDoc.Descendants(), new List<string> { name });
		}

		/// <summary>
		/// Gets the specified element from an element sequence.
		/// </summary>
		/// <param name="els">The element sequence to search.</param>
		/// <param name="names">The list of names that contain the element to find, starting with the highest parent.</param>
		/// <returns>The specified element from the element sequence.</returns>
		private static XElement GetElement(this IEnumerable<XElement> els, List<string> names)
		{
			return GetElement(els, names, 0, true);
		}

		/// <summary>
		/// Gets the specified element from a document.
		/// </summary>
		/// <param name="xDoc">The document to search.</param>
		/// <param name="name">The name of the element to find.</param>
		/// <param name="attribute">The attribute that identifies the element to find.</param>
		/// <returns>The specified element from the document.</returns>
		public static XElement GetElement(this XDocument xDoc, string name, KeyValuePair<string, string> attribute)
		{
			return GetElement(xDoc.Descendants(), name, attribute);
		}

		/// <summary>
		/// Gets the specified element from an element sequence.
		/// </summary>
		/// <param name="els">The element sequence to search.</param>
		/// <param name="name">The name of the element to find.</param>
		/// <param name="attribute">The attribute that identifies the element to find.</param>
		/// <returns>The specified element from the element sequence.</returns>
		private static XElement GetElement(this IEnumerable<XElement> els, string name, KeyValuePair<string, string> attribute)
		{
			return GetElement(els, new List<string> { name }, new List<KeyValuePair<string, string>> { attribute });
		}

		/// <summary>
		/// Gets the specified element from an element sequence.
		/// </summary>
		/// <param name="els">The element sequence to search.</param>
		/// <param name="names">The list of names that contain the element to find, starting with the highest parent.</param>
		/// <param name="attributes">The list of attributes that identify a specific element.</param>
		/// <returns>The specified element from the document.</returns>
		private static XElement GetElement(this IEnumerable<XElement> els, List<string> names, List<KeyValuePair<string, string>> attributes)
		{
			return GetElement(els, names, attributes, 0, true);
		}

		/// <summary>
		/// Gets the specified element from a document. A return value indicates whether the retrieval succeeded.
		/// </summary>
		/// <param name="xDoc">The document to search.</param>
		/// <param name="name">The name of the element to find.</param>
		/// <param name="element">The specified element from the document.</param>
		/// <returns>
		/// <c>true</c> if <paramref name="name"/> was retrieved successfully; otherwise, false.
		/// </returns>
		public static bool TryGetElement(this XDocument xDoc, string name, out XElement element)
		{
			return TryGetElement(xDoc, new List<string> { name }, out element);
		}

		/// <summary>
		/// Gets the specified element from a document.  A return value indicates whether the retrieval succeeded.
		/// </summary>
		/// <param name="xDoc">The document to search.</param>
		/// <param name="names">The list of names that contain the element to find, starting with the highest parent.</param>
		/// <param name="element">The specified element from the document.</param>
		/// <returns>
		/// <c>true</c> if <paramref name="names"/> was retrieved successfully; otherwise, false.
		/// </returns>
		private static bool TryGetElement(this XDocument xDoc, List<string> names, out XElement element)
		{
			return TryGetElement(xDoc.Descendants(), names, out element);
		}

		/// <summary>
		/// Gets the specified element from a element sequence.  A return value indicates whether the retrieval succeeded.
		/// </summary>
		/// <param name="els">The element sequence to search.</param>
		/// <param name="names">The list of names that contain the element to find, starting with the highest parent.</param>
		/// <param name="element">The specified element from the document.</param>
		/// <returns>
		/// <c>true</c> if <paramref name="names"/> was retrieved successfully; otherwise, false.
		/// </returns>
		public static bool TryGetElement(this IEnumerable<XElement> els, List<string> names, out XElement element)
		{
			return (element = GetElement(els, names, 0, false)) != null;
		}

		/// <summary>
		/// Gets the value of the specified element.
		/// </summary>
		/// <param name="xEl">The element that contains the value.</param>
		public static string GetValue(this XElement xEl)
		{
			return xEl == null ? string.Empty : xEl.Value;
		}

		/// <summary>
		/// Gets the specified element from a document.
		/// </summary>
		/// <param name="els">The sequence of elements to search.</param>
		/// <param name="names">The list of names that contain the element to find, starting with the highest parent.</param>
		/// <param name="attributes">The list of attributes that identify a specific element.</param>
		/// <param name="depth">The current level to search.</param>
		/// <param name="throwException">if set to <c>true</c>, an exception is thrown if the element does not exist.
		/// if set to <c>false</c>, null or the default value is returned if the element does not exist.</param>
		/// <returns>The specified element from the document.</returns>
		private static XElement GetElement(IEnumerable<XElement> els,
											List<string> names,
											List<KeyValuePair<string, string>> attributes,
											int depth,
											bool throwException)
		{
			XElement el;
			if (attributes[depth].Key != null)
			{
				var d = depth;
				el = els.GetSingleElement(
						throwException,
						e => e.Name == names[d]
										&& e.Attribute(attributes[d].Key) != null
										&& e.Attribute(attributes[d].Key).Value.EqualsIgnoreCase(attributes[d].Value));
			}
			else
			{
				var d = depth;
				el = els.GetSingleElement(
						throwException,
						e => e.Name == names[d]);
			}

			if (el == null)
			{
				return null;
			}

			var nestedEls = el.Descendants().ToList();
			return nestedEls.Count == 0 ? el : GetElement(nestedEls, names, attributes, ++depth, throwException);
		}

		/// <summary>
		/// Gets the specified element from a document.
		/// </summary>
		/// <param name="els">The sequence of elements to search.</param>
		/// <param name="names">The list of names that contain the element to find, starting with the highest parent.</param>
		/// <param name="depth">The current level to search.</param>
		/// <param name="throwException">if set to <c>true</c>, an exception is thrown if the element does not exist.
		/// if set to <c>false</c>, null or the default value is returned if the element does not exist.</param>
		/// <returns>The specified element from the document.</returns>
		private static XElement GetElement(IEnumerable<XElement> els,
											IList<string> names,
											int depth,
											bool throwException)
		{
			var d = depth;
			var el = els.GetSingleElement(throwException, e => e.Name == names[d]);

			if (el == null)
			{
				return null;
			}

			var nestedEls = el.Descendants().ToList();
			return nestedEls.Count == 0 ? el : GetElement(nestedEls, names, ++depth, throwException);
		}

		/// <summary>
		/// Gets a single element from a sequence of elements.
		/// </summary>
		/// <param name="els">The sequence of elements to search.</param>
		/// <param name="throwException">if set to <c>true</c>, an exception is thrown if the element does not exist.
		/// if set to <c>false</c>, null or the default value is returned if the element does not exist.</param>
		/// <param name="condition">The condition required to identify the element to return.</param>
		/// <returns>The element in the sequence that matches the condition.</returns>
		private static XElement GetSingleElement(this IEnumerable<XElement> els,
												bool throwException,
												Func<XElement, bool> condition)
		{
			return throwException ? els.Single(condition) : els.FirstOrDefault(condition);
		}

		/// <summary>
		/// Gets the value of the specified element attribute.
		/// </summary>
		/// <param name="e">The XElement from which to retrieve the attribute value.</param>
		/// <param name="name">The name of the attribute.</param>
		/// <returns>If the attribute is null, then empty string; else the attribute value.</returns>
		public static string GetAttributeValue(this XElement e, string name)
		{
			return e.Attribute(name).GetValue();
		}

		/// <summary>
		/// Gets the value of the specified element attribute. A return value indicates whether the retrieval succeeded.
		/// </summary>
		/// <param name="e">The XElement from which to retrieve the attribute value.</param>
		/// <param name="name">The name of the attribute.</param>
		/// <param name="value">The value from the specified element attribute.</param>
		/// <returns>
		/// If the attribute or its value is null or empty string, returns false;
		///     else returns true and sets the out param.
		/// </returns>
		public static bool TryGetAttributeValue(this XElement e, string name, out string value)
		{
			value = e.GetAttributeValue(name);
			return !value.IsNullOrEmptyTrimmed();
		}

		/// <summary>
		/// Gets the value of the specified attribute.
		/// </summary>
		/// <param name="a">The XAttribute from which to retrieve the string value.</param>
		/// <returns>If the attribute is null, then empty string; else the attribute value.</returns>
		private static string GetValue(this XAttribute a)
		{
			return a == null ? string.Empty : a.Value;
		}

		/// <summary>
		/// Gets an element from an XDocument using the specified XPath string. A return value indicates whether the retrieval succeeded.
		/// </summary>
		/// <param name="doc">The XDocument from which to retrieve the target element.</param>
		/// <param name="xPath">The XPath string to the target element.</param>
		/// <param name="el">The target element.</param>
		/// <returns></returns>
		public static bool TryGetXPathElement(this XDocument doc, string xPath, out XElement el)
		{
			el = doc.XPathSelectElement(xPath);
			return el != null;
		}
	}
}