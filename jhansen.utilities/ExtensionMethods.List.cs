﻿using System.Collections.Generic;

namespace JHansen.Utilities
{
	public static partial class ExtensionMethods
	{
		public static List<object> AddMultiple(this List<object> o1, params object[] listOfO)
		{
			foreach (object o in listOfO)
			{
				o1.Add(o);
			}

			return o1;
		}

		/// <summary>
		/// Adds an item to the list if it isn't already contained in it.
		/// </summary>
		public static void AddIfNew<T>(this IList<T> list, T item)
		{
			if (!list.Contains(item))
			{
				list.Add(item);
			}
		}
	}
}