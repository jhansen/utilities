﻿using System;
using System.Collections;
using System.Collections.Generic;
using JHansen.Utilities.Reflection;
using System.Linq;
using System.Text.RegularExpressions;

namespace JHansen.Utilities
{
	public static partial class ExtensionMethods
	{
        // this regex allows for simple nested bopn's that are indicated by simply prefixing them with a tilde
        // or for more complicated nested bopn's with embedded spaces and whatnot by wrapping them in parens, like: ~(Nested.Bopn.Here) 
		private static readonly Regex NestedBopn = new Regex(@"~(\((?<bopn>(?>\((?<c>)|[^()]+|\)(?<-c>))*(?(c)(?!)))\)|(?<bopn>[\w.]+))", RegexOptions.Compiled | RegexOptions.ExplicitCapture);
		
		private static string ReplaceNestedBopns<T>(Match match, IEnumerable<T> objects, object flatValues)
		{
            // RECURSION!! this is to handle bopn's nested within nested bopns
		    string bopn = NestedBopn.Replace(match.Groups["bopn"].Value, m => ReplaceNestedBopns(m, objects, flatValues));
			string value = Convert.ToString(objects.GetPropertyValue(bopn, flatValues));
			return string.IsNullOrEmpty(value)
				? " "
				: value;
		}
		
		public static PropertyEvaluationInfo GetPropertyEvalInfo<T>(this IEnumerable<T> objects, string pathToValue, object flatValues = null, bool createChain = false)
        {
			PropertyEvaluationInfo propEvalInfo = new PropertyEvaluationInfo();
			pathToValue = NestedBopn.Replace(pathToValue, m => ReplaceNestedBopns(m, objects, flatValues));

			T[] businessObjects = objects?.ToArray();
			NamedParameters namedParameters = businessObjects?.OfType<NamedParameters>().FirstOrDefault();

			if (businessObjects?.Length > 0 || flatValues != null)
			{
                // if the path string contains periods, assume the first part is the type of object or dictionary key
                string pathRoot = pathToValue.Split('.').First();

				object obj = null;

			    if (businessObjects != null)
			    {
			        obj = businessObjects.SingleOrDefault(x => x?.GetType().Name == pathRoot);
			    }
                obj = obj ?? flatValues; // if no object in the list matches the path, we'll check for it in flatValues

				if (obj != null)
				{
					propEvalInfo = obj.GetPropertyEvalInfo(pathToValue, namedParameters, createChain);
				}
			} 
			return propEvalInfo;
		}

        public static object GetPropertyValue<T>(this IEnumerable<T> objects, string pathToValue, object flatValues = null)
        {
	        return objects.GetPropertyEvalInfo(pathToValue, flatValues)?.GetValue();
        }

		public static Func<T, bool> ToLinqPredicate<T>(this Func<T, bool?> nullablePredicate)
		{
			return x => nullablePredicate(x) == true;
		}

		/// <summary>
		/// Custom extension to call the corresponding LINQ extension using a predicate that returns a bool?; this
		/// allows for use of the null-conditional operator in the predicate. Items for which the predicate
		/// returns null or false will be excluded.
		/// </summary>
		public static bool Any<T>(this IEnumerable<T> source, Func<T, bool?> nullablePredicate)
		{
			return source.Any(nullablePredicate.ToLinqPredicate());
		}

		/// <summary>
		/// Custom extension to call the corresponding LINQ extension using a predicate that returns a bool?; this
		/// allows for use of the null-conditional operator in the predicate. Items for which the predicate
		/// returns null or false will be excluded.
		/// </summary>
		public static bool All<T>(this IEnumerable<T> source, Func<T, bool?> nullablePredicate)
		{
			return source.All(nullablePredicate.ToLinqPredicate());
		}

		/// <summary>
		/// Custom extension to call the corresponding LINQ extension using a predicate that returns a bool?; this
		/// allows for use of the null-conditional operator in the predicate. Items for which the predicate
		/// returns null or false will be excluded.
		/// </summary>
		public static IEnumerable<T> Where<T>(this IEnumerable<T> source, Func<T, bool?> nullablePredicate)
		{
			return source.Where(nullablePredicate.ToLinqPredicate());
		}

		/// <summary>
		/// Custom extension to call the corresponding LINQ extension using a predicate that returns a bool?; this
		/// allows for use of the null-conditional operator in the predicate. Items for which the predicate
		/// returns null or false will be excluded.
		/// </summary>
		public static T First<T>(this IEnumerable<T> source, Func<T, bool?> nullabledPredicate)
		{
			return source.First(nullabledPredicate.ToLinqPredicate());
		}

		/// <summary>
		/// Custom extension to call the corresponding LINQ extension using a predicate that returns a bool?; this
		/// allows for use of the null-conditional operator in the predicate. Items for which the predicate
		/// returns null or false will be excluded.
		/// </summary>
		public static T FirstOrDefault<T>(this IEnumerable<T> source, Func<T, bool?> nullabledPredicate)
		{
			return source.FirstOrDefault(nullabledPredicate.ToLinqPredicate());
		}

		/// <summary>
		/// Custom extension to call the corresponding LINQ extension using a predicate that returns a bool?; this
		/// allows for use of the null-conditional operator in the predicate. Items for which the predicate
		/// returns null or false will be excluded.
		/// </summary>
		public static T Last<T>(this IEnumerable<T> source, Func<T, bool?> nullabledPredicate)
		{
			return source.Last(nullabledPredicate.ToLinqPredicate());
		}

		/// <summary>
		/// Custom extension to call the corresponding LINQ extension using a predicate that returns a bool?; this
		/// allows for use of the null-conditional operator in the predicate. Items for which the predicate
		/// returns null or false will be excluded.
		/// </summary>
		public static T LastOrDefault<T>(this IEnumerable<T> source, Func<T, bool?> nullabledPredicate)
		{
			return source.LastOrDefault(nullabledPredicate.ToLinqPredicate());
		}
	}
}