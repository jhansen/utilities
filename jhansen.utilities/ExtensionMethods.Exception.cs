﻿using System;
using System.Linq;
using System.Text;

namespace JHansen.Utilities
{
    public static partial class ExtensionMethods
    {
		public static string GetSummary(this Exception exception)
		{
			StringBuilder summary = new StringBuilder();

			summary.AppendLine(exception.GetBaseException().GetType().FullName);
			summary.AppendLine(exception.Message);
			summary.Append(exception.StackTrace);

			var aggregateException = exception as AggregateException;
			if (aggregateException != null && aggregateException.InnerExceptions.Any())
			{
				summary.AppendLine();
				foreach (var ex in aggregateException.InnerExceptions)
				{
					summary.Append(ex.GetSummary());
				}
			}
			else if (exception.InnerException != null)
			{
				summary.AppendLine();
				summary.Append(exception.InnerException.GetSummary());
			}

			return summary.ToString();
		}
	}
}
