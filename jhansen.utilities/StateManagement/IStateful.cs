﻿namespace JHansen.Utilities.StateManagement
{
	public interface IStateful
	{
		void FromValue(object value);
		object GetValue();
		ValueState? State { get; set; }
	}
}