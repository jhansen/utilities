﻿using System;
using Newtonsoft.Json;

namespace JHansen.Utilities.StateManagement
{
	public static class Stateful
	{
		public const string NoneText = "NONE";
		public const string UnknownText = "UNKNOWN";
		public const string HiddenText = "_H!DDEN";
		public const string RemovedText = "_REM0VED";
	}

	[Serializable, JsonConverter(typeof(StatefulJsonConverter))]
	public class Stateful<T> : IStateful, IConvertible
	{
		public Stateful() {}

		public Stateful(T value)
		{
			FromValue(value);
		}

		public static implicit operator Stateful<T>(T value)
		{
			return new Stateful<T>(value);
		}

		public static implicit operator T(Stateful<T> statefulValue)
		{
			return statefulValue != null
				? statefulValue.Value
				: default(T);
		}

		public delegate void ValueChangeEventHandler(Stateful<T> sender, ValueChangeEventArgs<T> e);

		public event ValueChangeEventHandler OnValueChanging;

		private ValueChangeEventArgs<T> ValueChanging(T newValue, T oldValue, ValueState? currentState)
		{
			ValueChangeEventArgs<T> args = new ValueChangeEventArgs<T>(newValue, oldValue, currentState);

			if (OnValueChanging != null)
			{
				OnValueChanging(this, args);
			}

			return args;
		}

		private T _value;
		private ValueState? _state;

		public T Value
		{
			get { return _value; }
			set
			{
				var valueChangingArgs = ValueChanging(value, _value, State);
				_value = valueChangingArgs.NewValue;

				if (!valueChangingArgs.PreventStateChange)
				{
					State = ValueState.Visited;
				}
			}
		}

		public object GetValue()
		{
			return _value;
		}

		public ValueState? State
		{
			get { return _state; }
			set
			{
				_state = value;
				if (_state.HasValue 
					&& (_state == ValueState.None || _state == ValueState.Unknown || _state == ValueState.Removed))
				{
					_value = default(T);
				}
			}
		}
		
		public override string ToString()
		{
			string value = string.Empty;

			if (State.HasValue 
				&& (State.Value == ValueState.None || State.Value == ValueState.Unknown))
			{
				value = State.Value.ToString().ToUpper();
			}
			else if (Value != null)
			{
				value = Value.ToString();
			}

			return value;
		}

		public void FromValue(object value)
		{
			if (value != null)
			{
				Type valueType = value.GetType();
				if (valueType == typeof (string))
				{
					FromString((string) value);
				}
				else if (valueType == typeof (T)
					|| (typeof(T).IsNullable() && valueType == Nullable.GetUnderlyingType(typeof(T))))
				{
					Value = (T) value;
				}
				else if (valueType == typeof (Stateful<T>))
				{
					var statefulVal = (Stateful<T>)value;
					Value = statefulVal.Value;
					State = statefulVal.State;
				}
				else if (valueType == typeof (ValueState) || valueType == typeof (ValueState?))
				{
					State = (ValueState?) value;
				}
				else
				{
					throw new InvalidCastException(string.Format("Cannot convert value of type {0} to {1}", valueType, this.GetType()));
				}
			}
		}
		
		public void FromString(string value)
		{
			if (value != null)
			{
				switch (value.ToUpper())
				{
					case Stateful.UnknownText:
						State = ValueState.Unknown;
						break;
					case Stateful.NoneText:
						State = ValueState.None;
						break;
					case Stateful.HiddenText:
						State = ValueState.Hidden;
						break;
					case Stateful.RemovedText:
						State = ValueState.Removed;
						break;
					default:
						Value = value.ConvertTo<T>();
						break;
				}
			}
			else
			{
				Value = default(T);
			}
		}

		#region IConvertible implementation

		TypeCode IConvertible.GetTypeCode()
		{
			return TypeCode.Object;
		}

		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(Value);
		}

		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(Value);
		}

		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(Value);
		}

		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(Value);
		}

		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(Value);
		}

		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(Value);
		}

		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(Value);
		}

		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(Value);
		}

		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(Value);
		}

		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(Value);
		}

		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(Value);
		}

		string IConvertible.ToString(IFormatProvider provider)
		{
			return Convert.ToString(Value);
		}

		object IConvertible.ToType(Type conversionType, IFormatProvider provider)
		{
			return Convert.ChangeType(Value, conversionType);
		}

		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(Value);
		}

		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(Value);
		}

		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(Value);
		}

		#endregion
	}
}
