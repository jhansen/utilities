﻿namespace JHansen.Utilities.StateManagement.Extensions
{
	public static class StatefulExtensions
	{
		/// <summary>
		/// Returns true only if the value is not null and the inner nullable bool is True.
		/// </summary>
		public static bool IsTrue(this Stateful<bool?> value)
		{
			return value?.Value ?? false;
		}

		public static bool IsInvisible(this ValueState? state)
		{
			return state.HasValue && (state == ValueState.Hidden || state == ValueState.Removed);
		}
	}
}
