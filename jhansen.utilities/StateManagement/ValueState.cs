﻿namespace JHansen.Utilities.StateManagement
{
	public enum ValueState
	{
		NotSet = 0,
		None = 1,
		Unknown = 2,
		Visited = 3,
		Hidden = 4,
		Removed = 5
	}
}
