﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace JHansen.Utilities.StateManagement
{
	public class StatefulJsonConverter : JsonConverter
	{
		private const string ValueKey = "value";
		private const string StateKey = "state";
	    private const string StringValueKey = "stringvalue";

        public override bool CanConvert(Type objectType)
		{
			return objectType.IsStateful();
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			IStateful stateful = objectType.GetDefaultValue() as IStateful;
			JObject jsonObject = JObject.Load(reader);
		    if (jsonObject[StringValueKey] != null)
		    {
		        stateful.FromValue(jsonObject[StringValueKey].Value<string>());
            }
		    else
		    {
		        string state = jsonObject[StateKey] != null
		            ? jsonObject[StateKey].Value<string>()
		            : null;

		        stateful.FromValue(jsonObject[ValueKey].Value<string>());
		        stateful.State = state.IsNullOrEmptyTrimmed()
		            ? null
		            : (ValueState?)Enum.Parse(typeof(ValueState), state);
            }
			
			return stateful;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			IStateful stateful = value as IStateful;

			writer.WriteStartObject();

			if (stateful != null)
			{
				writer.WritePropertyName(ValueKey);
				writer.WriteValue(stateful.GetValue());
				if (stateful.State.HasValue)
				{
					writer.WritePropertyName(StateKey);
					writer.WriteValue(stateful.State.Value.ToString());	
				}
			}

			writer.WriteEndObject();
		}
	}
}
