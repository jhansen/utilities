﻿using System;

namespace JHansen.Utilities.StateManagement
{
	public class ValueChangeEventArgs<T> : EventArgs
	{
		public ValueChangeEventArgs(T newValue, T oldValue, ValueState? currentState)
		{
			NewValue = newValue;
			OldValue = oldValue;
			CurrentState = currentState;
		}

		public T OldValue { get; private set; }
		public T NewValue { get; set; }
		public ValueState? CurrentState { get; private set; }

		public bool PreventStateChange { get; set; }
	}
}
