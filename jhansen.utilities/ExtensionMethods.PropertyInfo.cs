﻿using System.Reflection;
using JHansen.Utilities.StateManagement;

namespace JHansen.Utilities
{
	public static partial class ExtensionMethods
	{
		public static object GetValue(this PropertyInfo propertyInfo, object obj, bool createIfNull)
		{
			if (propertyInfo == null)
			{
				return null;
			}

			object value = propertyInfo.GetValue(obj);
			if (value == null && createIfNull && propertyInfo.CanWrite)
			{
				value = propertyInfo.PropertyType.GetDefaultValue();
				propertyInfo.SetValue(obj, value);
			}
			return value;
		}

		public static bool TrySetValue(this PropertyInfo propertyInfo, object parent, object value)
		{
			IStateful statefulValue = null;
			if (propertyInfo.PropertyType.IsStateful())
			{
				statefulValue = propertyInfo.GetValue(parent) as IStateful;
			}

			bool success = false;
			if (statefulValue != null)
			{
				statefulValue.FromValue(value);
				success = true;
			}
			else if (propertyInfo.CanWrite)
			{
				propertyInfo.SetValue(parent, value);
				success = true;
			}
			return success;
		}

		public static bool CanTrySetValue(this PropertyInfo propertyInfo)
		{
			return propertyInfo.CanWrite || propertyInfo.PropertyType.IsStateful();
		}
	}
}