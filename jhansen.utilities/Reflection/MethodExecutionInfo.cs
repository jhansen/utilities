﻿using System;
using System.Linq;
using System.Reflection;

namespace JHansen.Utilities.Reflection
{
	public class MethodExecutionInfo
	{
		public MethodExecutionInfo(MethodInfo method)
		{
			Method = method;
		}

		internal MethodInfo Method { get; }
		
		public object[] Arguments { get; set; }

		public bool SuccessfullyEvaluated => Method != null;

		public Type ReturnType => Method?.ReturnType;

		public object Execute(object target)
		{
			// Execute the method, passing the target as the first argument for extension methods
			return Method?.Invoke(target, 
				Method.DeclaringType?.IsInstanceOfType(target) == true
					? Arguments
					: new[] { target }.Concat(Arguments).ToArray());
		}
	}
}
