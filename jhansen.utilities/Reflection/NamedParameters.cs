﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace JHansen.Utilities.Reflection
{
    [Serializable]
	public class NamedParameters : Dictionary<string, object>
	{
		public NamedParameters() { }
		public NamedParameters(int capacity): base(capacity) { }
		public NamedParameters(IEqualityComparer<string> comparer) : base(comparer) { }
		public NamedParameters(IDictionary<string, object> dictionary) : base(dictionary) { }
		public NamedParameters(int capacity, IEqualityComparer<string> comparer) : base(capacity, comparer) { }
		public NamedParameters(SerializationInfo info, StreamingContext context) : base(info, context) { }
		public NamedParameters(IDictionary<string, object> dictionary, IEqualityComparer<string> comparer) : base(dictionary, comparer) { }
	}
}
