﻿using System;

namespace JHansen.Utilities.Reflection
{
	public class PropertyEvaluationInfo
	{
		public string Source { get; set; }
		public object Parent { get; set; }
		public string FormatString { get; set; }

		public Func<object> GetRawValue { get; set; }
		internal Func<object, bool> TrySetValue { get; set; }

		public object GetValue()
		{
			object value = GetRawValue?.Invoke();

			return !String.IsNullOrEmpty(FormatString) && typeof (IFormattable).IsAssignableFrom(ValueType)
				? ((IFormattable)value)?.ToString(FormatString, null)
				: value;
		}

		public bool SetValue(object value)
		{
			return TrySetValue?.Invoke(value) ?? false;
		}

		public bool CanSetValue => TrySetValue != null;

		private Type _valueType;
		public Type ValueType
		{
			get
			{
				return _valueType;
			}
			internal set
			{
				_valueType = value;
			}
		}

		public bool SuccessfullyEvaluated { get; set; }

        public ReflectedMemberType MemberType { get; set; }
		public string Error { get; set; }
	}
}
