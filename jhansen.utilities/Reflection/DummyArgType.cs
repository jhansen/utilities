﻿namespace JHansen.Utilities.Reflection
{
	/// <summary>
	/// This class is used simply as a placeholder type in the Type.GetMatchingMethod() extension method.
	/// </summary>
	internal sealed class DummyArgType
	{
	}
}
