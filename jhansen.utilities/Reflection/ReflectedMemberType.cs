﻿namespace JHansen.Utilities.Reflection
{
    public enum ReflectedMemberType
    {
        None = 0,
        Property,
        Method,
        Dictionary,
        FilteredList,
        FilteredListItem
    }
}
