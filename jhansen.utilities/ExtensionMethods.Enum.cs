﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace JHansen.Utilities
{
	public static partial class ExtensionMethods
	{
		
		/// <summary>
		/// Determines whether the flag is set in the specified enumeration value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value">The enumeration value to check.</param>
		/// <param name="flag">The flag to be identified.</param>
		/// <returns>
		/// 	<c>true</c> if the flag is set in the specified enumeration value; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsFlagSet<T>(this T value, T flag) where T : struct
		{
			return Attribute.IsDefined(typeof(T), typeof(FlagsAttribute))
				? (Convert.ToInt64(value) & Convert.ToInt64(flag)) != 0
				: false;
		}

	    public static SelectList ToSelectList<TEnum>(this TEnum enumObj)
	    {
	        var values = from TEnum e in Enum.GetValues(typeof(TEnum))
	            select new {ID = e, Name = e.ToString()};

	        return new SelectList(values, "Id", "Name", enumObj);
	    }
	}
}