﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace JHansen.Utilities
{
	/// <summary>
	/// Class to encapsulate various extension methods
	/// </summary>
	public static partial class ExtensionMethods
	{
		private static TraceSource _traceSource = new TraceSource("JHansen.Utilities.ExtensionMethods");

		public static int TryParseInt(this string value)
		{
			int result;

			return int.TryParse(value, out result) ? result : 0;
		}

		/// <summary>
		/// Performs the specified action on each element of IEnumerable&lt;T&gt;.
		/// </summary>
		/// <typeparam name="T">Type of item in IEnumerable collection</typeparam>
		/// <param name="sequence">Collection of items to be acted on by Action&lt;T&gt; delegate.</param>
		/// <param name="action">The Action&lt;T&gt; delegate to perform on each element of IEnumerable&lt;T&gt;.</param>
		public static void ForEach<T>(this IEnumerable<T> sequence, Action<T> action)
		{
			foreach (var item in sequence)
				action(item);
		}

		public static bool In(this string source, params string[] args)
		{
		    return args != null && args.Any(s => s.EqualsIgnoreCase(source, true));
		}

	    public static bool IsBetween(this DateTime source, DateTime firstDate, DateTime secondDate)
		{
			return (source >= firstDate && secondDate >= source) || (source >= secondDate && firstDate >= source);
		}
		
		public static string GetDigitsOnly(this string source)
		{
			if (string.IsNullOrWhiteSpace(source))
				return string.Empty;

			return Regex.Replace(source, @"[^\d]+", string.Empty, RegexOptions.Compiled);
		}
		
		/// <summary>
		/// Returns the lesser of two values
		/// </summary>
		public static T Min<T>(this T left, T right)
			where T : IComparable
		{
			return left.CompareTo(right) < 0
				? left
				: right;
		}

		/// <summary>
		/// Returns the greater of two values
		/// </summary>
		public static T Max<T>(this T left, T right)
			where T : IComparable
		{
			return left.CompareTo(right) > 0
				? left
				: right;
		}

		public class Utf8StringWriter : StringWriter
		{
			public override Encoding Encoding
			{
				get { return Encoding.UTF8; }
			}
		}

		public static string Serialize<T>(this T value)
		{
			if (value == null)
			{
				return string.Empty;
			}

			try
			{
				var xmlserializer = new XmlSerializer(typeof(T));
				StringWriter stringWriter = new Utf8StringWriter();

				using (var writer = XmlWriter.Create(stringWriter))
				{
					xmlserializer.Serialize(writer, value);
					return stringWriter.ToString();
				}
			}
			catch (Exception ex)
			{
				throw new Exception("An error occurred", ex);
			}
		}

		public static bool IsDefault<TValue>(this TValue value)
		{
			return EqualityComparer<TValue>.Default.Equals(value, default(TValue));
		}
	}
}