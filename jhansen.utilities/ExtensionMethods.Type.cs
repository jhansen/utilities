﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using JHansen.Utilities.Reflection;
using JHansen.Utilities.StateManagement;

namespace JHansen.Utilities
{
	public static partial class ExtensionMethods
	{
		public static readonly Type[] NumericTypes = {
			typeof(short), typeof(int), typeof(long), typeof(decimal), typeof(float), typeof(double),
			typeof(ushort), typeof(uint), typeof(ulong)
		};

		/// <summary>
		/// Returns true if the given type is one of the .NET number types(<see cref="NumericTypes"/>), OR a Nullable version of one,
		/// otherwise false.
		/// </summary>
		public static bool IsNumeric(this Type type)
		{
			if (type == null)
			{
				return false;
			}
			Type actual = type.IsNullable()
				? Nullable.GetUnderlyingType(type)
				: type;
			return NumericTypes.Contains(actual);
		}

		/// <summary>
		/// Creates a generic type string for the specified type.
		/// </summary>
		/// <param name="t">The specified type.</param>
		/// <returns>A generic type string for the specified type.</returns>
		/// <remarks>http://stackoverflow.com/questions/2448800/</remarks>
		public static string ToGenericTypeString(this Type t)
		{
			if (!t.IsGenericType)
			{
				return t.Name;
			}

			string genericTypeName = t.GetGenericTypeDefinition().Name;
			genericTypeName = genericTypeName.Substring(0, genericTypeName.IndexOf('`'));

			string genericArgs = string.Join(",", t.GetGenericArguments().Select(ta => ToGenericTypeString(ta)).ToArray());

			return genericTypeName + "<" + genericArgs + ">";
		}

		private static readonly ConcurrentDictionary<MethodSignatureKey, Func<object>> ConstructorCache = new ConcurrentDictionary<MethodSignatureKey, Func<object>>();
		
		/// <summary>
		/// Uses Expressions to get a function to execute the default (parameterless) constructor
		/// of a type. If the type does not have a parameterless constructor (or if it is a value type)
		/// this method will return null.
		/// </summary>
		/// <remarks>The reflection and Expression compilation will be done the first time this method is called for each
		/// Type, and subsequent calls for the same Type will use a local static cache to retrieve the Func.</remarks>
		public static Func<object> GetDefaultConstructor(this Type type)
		{
			MethodSignatureKey key = new MethodSignatureKey(type);
			
			return ConstructorCache.GetOrAdd(key, k => 
				type.HasDefaultConstructor()
					? Expression.Lambda<Func<object>>(Expression.New(type)).Compile()
					: null);
		}

		/// <summary>
		/// Uses reflection to determine if the given type has a parameterless constructor.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static bool HasDefaultConstructor(this Type type)
		{
			return type.GetConstructor(Type.EmptyTypes) != null;
		}
		
		/// <summary>
		/// Returns true if the given type is a Nullable type.
		/// </summary>
		public static bool IsNullable(this Type type)
		{
			return (type != null) && type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
		}

		public static bool IsStateful(this Type type)
		{
			return (type != null) && type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Stateful<>);
		}

	    public static bool IsNullableEnum(this Type type)
	    {
	        return (type != null)
	               && type.IsGenericType
	               && type.GetGenericTypeDefinition() == typeof(Nullable<>)
	               && type.GenericTypeArguments.Any(x => x.IsEnum);
	    }

		/// <summary>
		/// Attempts to convert the given value to the specified type, using Parse where possible.
		/// Special handling is used for Guid, Enum, and IConvertible types.
		/// </summary>
		public static T ConvertTo<T>(this object value)
		{
			return (T)value.ConvertTo(typeof(T));
		}

		/// <summary>
		/// Attempts to convert the given value to the specified type, using Parse where possible.
		/// Special handling is used for Guid, Enum, and IConvertible types.
		/// </summary>
		public static object ConvertTo(this object value, Type type)
		{
			object result = null;

			if (type.IsInstanceOfType(value))
			{
				result = value;
			}
			else if (type == typeof(Guid))
			{
				Guid guid;
				result = (value != null && Guid.TryParse(value.ToString(), out guid))
					? guid : Guid.Empty;
			}
			else
			{
				if (type.IsStateful())
				{
					result = type.GetDefaultConstructor()();
					((IStateful)result).FromValue(value);
				}
				else
				{
					if (value != null)
					{
						if (type.IsEnum)
						{
							if (!EnumHelper.TryParse(type, value.ToString(), true, out result))
							{
								result = type.GetDefaultValue();
							}
						}
                        else if (type.IsNullableEnum())
					    {
                            EnumHelper.TryParse(type.GenericTypeArguments.FirstOrDefault(x => x.IsEnum), value.ToString(),
                                        true, out result);
                        }
                        else if (type.IsNullable())
						{
						    result = (value is string && ((string)value).IsNullOrEmptyTrimmed())
									? null
									: type.GetGenericArguments().First().Parse(value);
						}
						else
						{
						    if (type.IsAssignableFrom(value.GetType()) && value is IConvertible)
						    {
						        result = Convert.ChangeType(value, type, null);
						    }
						    else
						    {
						        if (type.HasParse(value))
						        {
						            result = type.Parse(value);
						        }
						        else if (type == typeof(string))
						        {
						            result = value.ToString();
						        }
						        else if (type == typeof(TimeZoneInfo))
						        {
						            result = TimeZoneInfo.FindSystemTimeZoneById(value.ToString());
						        }
						        else
						        {
						            result = type.GetDefaultValue();
						        }
						    }
						}
					}
				}
			}

			return result;
		}

		/// <summary>
		/// Determines whether the given type has a Parse method that can be called with the given value.
		/// </summary>
		public static bool HasParse(this Type type, object value)
		{
			return type.GetMethods(Flags.AllMethodBindingFlags).Any(m => m.Name == "Parse" && CanParse(m, value));
		}

		private static bool CanParse(MethodInfo method, object value)
		{
			ParameterInfo[] parameters = method.GetParameters();
			Type firstParameter = parameters.First().ParameterType;
			Type valueType = value.GetType();

			bool valueIsString = valueType == typeof(string);
			bool valueIsEmptyString = valueIsString && value.ToString().IsNullOrEmptyTrimmed();

			bool canParse = parameters.Length == 1
				&& !valueIsEmptyString
				&& (
					firstParameter.IsAssignableFrom(valueType)
					||
					valueIsString
				);

			return canParse;
		}

		/// <summary>
		/// Finds the Parse method on the given type T, and calls it with the given value, returning the resulting instance of T.
		/// If the type doesn't have a matching Parse method, returns null.
		/// </summary>
		public static T Parse<T>(this Type type, object value)
		{
			return (T)type.Parse(value);
		}

		/// <summary>
		/// Finds the Parse method on the given type T, and calls it with the given value, returning the resulting instance of T.
		/// If the type doesn't have a matching Parse method, returns null.
		/// </summary>
		public static object Parse(this Type type, object value)
		{
			MethodInfo parse = type.GetMethods(Flags.AllMethodBindingFlags).FirstOrDefault(m => m.Name == "Parse" && CanParse(m, value));
			try
			{
				return (parse != null)
					? parse.Invoke(type, new[]
						{
							parse.GetParameters().First().ParameterType == typeof(string) 
								? value.ToString() 
								: value
						})
					: null;
			}
			catch (TargetInvocationException tex)
			{
				if (tex.InnerException != null)
				{
					throw tex.InnerException;
				}
				throw;
			}
		}

		private static readonly ConcurrentDictionary<Type, Func<object>> TypeDefaultFactoryCache = new ConcurrentDictionary<Type, Func<object>>();

		/// <summary>
		/// Attempts to return a non-null default value for the given type. String will return empty, value
		/// types will return default values, complex types will return the value of the default constructor (if
		/// one exists). If <param name="type"/> is null or a type without a default constructor, this will
		/// return null.
		/// </summary>
		public static object GetDefaultValue(this Type type)
		{
			if (type == null)
			{
				return null;
			}
			
			return TypeDefaultFactoryCache.GetOrAdd(type, t => {
				Func<object> defaultValueFactory;
				if (t == typeof(string))
				{
					// special default for strings because ... strings are special
					defaultValueFactory = () => string.Empty;
				}
				else if (t.IsValueType)
				{
					object defaultValue = Activator.CreateInstance(t);
					defaultValueFactory = () => defaultValue;
				}
				else
				{
					defaultValueFactory = GetDefaultConstructor(t)
						?? (() => null);
				}

				return defaultValueFactory;
			}).Invoke();
		}

		public static bool IsTypeOrSimilar<T>(this Type type) where T : struct
		{
			return type == typeof (T)
			       || type == typeof (Stateful<T>)
			       || type == typeof (T?)
			       || type == typeof (Stateful<T?>);
		}

		private static readonly ConcurrentDictionary<MethodSignatureKey, MethodInfo> MethodCache = new ConcurrentDictionary<MethodSignatureKey, MethodInfo>();

        /// <summary>
        /// Uses reflection to find a method on the given target type with the given name and taking the given arguments and generic type parameters.
        /// The result of the reflection is staticly cached to improve speed of subsequent calls for the same method.
        /// </summary>
        /// <param name="argsString">Comma-delimitted list of values to be passed as arguments to the method.</param>
        /// <param name="typeParamsString">Comma-delimitted list of type names for generic methods.</param>
        public static MethodExecutionInfo GetMatchingMethod(this Type target, string methodName, string argsString, string typeParamsString)
		{
			string[] argValues = string.IsNullOrEmpty(argsString)
				? new string[0]
				: argsString.Split(',').Select(s => s.Trim()).ToArray();

            string[] typeParams = string.IsNullOrEmpty(typeParamsString)
                ? new string[0]
                : typeParamsString.Split(',').Select(s => s.Trim()).ToArray();

            MethodSignatureKey key = new MethodSignatureKey(target, methodName,
                typeParams,
                Enumerable.Repeat(typeof (DummyArgType), argValues.Length).ToArray());

			MethodInfo match = MethodCache.GetOrAdd(key, k => k.FindMethod(argValues));

			// convert the argsString to the types of parameters needed to invoke the method
			// unfortunately, this is redundant effort the first time through, but that's a small price to pay for the benefit of caching the MethodInfo
			object[] convertedArgs;
			if (!match.TryConvertArguments(key.ParentType, argValues, out convertedArgs))
			{
				match = null; // the args from the string couldn't be parsed to the parameter types
			}

			return new MethodExecutionInfo(match)
			{
				Arguments = convertedArgs
			};
		}

        /// <summary>
        /// Determines if the Type satisfies the constraints for the generic type parameter specified by <paramref name="typeParameter"/>.
        /// </summary>
        public static bool SatisfiesGenericTypeConstraints(this Type proposedType, TypeInfo typeParameter)
        {
            return proposedType != null && typeParameter != null
                && typeParameter.BaseType?.IsAssignableFrom(proposedType) == true
                && typeParameter.ImplementedInterfaces.All(i => i.IsAssignableFrom(proposedType));
        }

        /// <summary>
        /// Scans all loaded assemblies looking for types matching the given name (where then type's FullName ends with the <paramref name="name"/>)
        /// that also satisfy the constraints of the given generic type parameter.
        /// </summary>
	    public static IEnumerable<Type> ScanAssembliesForMatchingTypes(this TypeInfo typeParameter, string name)
	    {
	        return AppDomain.CurrentDomain.GetExportedTypes()
	            .Where(type => type.Name.Length <= name.Length && type.FullName.EndsWith(name) && type.SatisfiesGenericTypeConstraints(typeParameter));
	    }

        /// <summary>
        /// <see cref="Type.IsAssignableFrom(Type)"/> - this is the same as that, but also works for generic types. 
        /// Taken and modified from https://stackoverflow.com/a/41665993 
        /// </summary>
        /// <param name="targetType">The type you want.</param>
        /// <param name="suspectType">The type that might be a match.</param>
        public static bool IsReallyAssignableFrom(this Type targetType, Type suspectType)
        {
            if (targetType == null)
            {
                throw new ArgumentNullException(nameof(targetType));
            }

            if (suspectType == null)
            {
                throw new ArgumentNullException(nameof(suspectType));
            }

            bool isMatch = false;

            if (targetType.IsGenericParameter) // e.g.: T
            {
                // The target type is a generic parameter. 

                // First, check if all special attribute constraints are respected.

                var constraintAttributes = targetType.GenericParameterAttributes;

                isMatch = constraintAttributes == GenericParameterAttributes.None
                    // e.g.: where T : struct
                    || (!constraintAttributes.HasFlag(GenericParameterAttributes.NotNullableValueTypeConstraint) || suspectType.IsValueType)
                    // e.g.: where T : class
                    && (!constraintAttributes.HasFlag(GenericParameterAttributes.ReferenceTypeConstraint) || !suspectType.IsValueType)
                    // e.g.: where T : new()
                    && (
                        (!constraintAttributes.HasFlag(GenericParameterAttributes.DefaultConstructorConstraint) || suspectType.HasDefaultConstructor())
                        || constraintAttributes.HasFlag(GenericParameterAttributes.NotNullableValueTypeConstraint));
                
                if (isMatch)
                {
                    // Then, check if all type constraints are respected.

                    // e.g.: where T : BaseType, IInterface1, IInterface2
                    foreach (var constraint in targetType.GetGenericParameterConstraints())
                    {
                        if (!constraint.IsAssignableFrom(suspectType))
                        {
                            isMatch = false;
                        }
                    }
                }
            }
            else if (targetType.ContainsGenericParameters)
            {
                // The target type is not a generic parameter but contains generic parameters.
                // It could be either a generic type or an array.

                if (targetType.IsGenericType) // e.g. Generic<T1, int, T2>
                {
                    // The target type is a generic type.

                    Type targetGenericDefinition = targetType.GetGenericTypeDefinition(); // e.g.: Generic<,,>
                    Type[] targetGenericArguments = targetType.GetGenericArguments(); // e.g.: { T1, int, T2 }

                    // Check a list of possible candidate closed-constructed types:
                    //  - the closed-constructed type itself
                    //  - its base type, if any (i.e.: if the closed-constructed type is not object)
                    //  - its implemented interfaces

                    List<Type> inheritedSuspectTypes = new List<Type> {suspectType};
                    
                    if (suspectType.BaseType != null)
                    {
                        inheritedSuspectTypes.Add(suspectType.BaseType);
                    }

                    inheritedSuspectTypes.AddRange(suspectType.GetInterfaces());

                    foreach (var inheritedSuspectType in inheritedSuspectTypes)
                    {
                        if (inheritedSuspectType.IsGenericType &&
                            inheritedSuspectType.GetGenericTypeDefinition() == targetGenericDefinition)
                        {
                            // The inherited suspect type and the target type share the same generic definition.

                            Type[] inheritedSuspectGenericArguments = inheritedSuspectType.GetGenericArguments(); // e.g.: { float, int, string }

                            // For each target generic argument, recursively check if it can be made into a suspect type via the suspect generic argument
                            isMatch = targetGenericArguments.Zip(inheritedSuspectGenericArguments, (t, s) => new {target = t, suspect = s})
                                .All(types => types.target.IsReallyAssignableFrom(types.suspect));
                            if (isMatch)
                            {
                                break;
                            }
                        }
                    }
                }
                else if (targetType.IsArray) // e.g. T[]
                {
                    // The target type is an array.

                    if (suspectType.IsArray &&
                        suspectType.GetArrayRank() == targetType.GetArrayRank())
                    {
                        Type openConstructedElementType = targetType.GetElementType();
                        Type closedConstructedElementType = suspectType.GetElementType();

                        isMatch = openConstructedElementType.IsReallyAssignableFrom(closedConstructedElementType);
                    }
                }
            }
            else
            {
                // The target type does not contain generic parameters, just check IsAssignableFrom

                isMatch = targetType.IsAssignableFrom(suspectType);
            }

            return isMatch;
        }

    }
}