﻿using System;
using System.Diagnostics;

namespace JHansen.Utilities.Diagnostics
{
	public static class TraceSourceExtensions
	{
		public static void TraceEvent(this TraceSource trace, TraceEventType eventType, TraceEvent eventId, string message)
		{
			trace.TraceEvent(eventType, (int)eventId, message.RemoveControlCharacters());
		}

		public static void TraceEvent(this TraceSource trace, TraceEventType eventType, TraceEvent eventId, Exception ex)
		{
			trace.TraceEvent(eventType, (int)eventId, ex.GetSummary().RemoveControlCharacters());
		}
	}
}
