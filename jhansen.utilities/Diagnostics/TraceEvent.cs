﻿namespace JHansen.Utilities.Diagnostics
{
	public enum TraceEvent
	{
		None = 0,
		DynamicLinqParseException = 1,
		DictionarySetValueException = 2
	}
}
