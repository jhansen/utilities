﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace JHansen.Utilities
{
	public struct MethodSignatureKey
	{
		public MethodSignatureKey(Type parentType, string methodName, string[] genericTypes, params Type[] argTypes)
		{
			ParentType = parentType;
			MethodName = methodName;
		    GenericTypes = genericTypes;
			ArgTypes = argTypes;
		}

		public MethodSignatureKey(Type parentType, string[] genericTypes = null, params Type[] argTypes) : this(parentType, "", genericTypes, argTypes)
		{
		}

		public readonly Type ParentType;
		public readonly string MethodName;
		public readonly Type[] ArgTypes;
	    public readonly string[] GenericTypes;

		public override bool Equals(object other)
		{
			bool isEqual = false;
			if (other is MethodSignatureKey)
			{
				MethodSignatureKey otherKey = (MethodSignatureKey)other;
				isEqual = ParentType == otherKey.ParentType
					&& MethodName == otherKey.MethodName
					&& ((IStructuralEquatable)ArgTypes).Equals(otherKey.ArgTypes, StructuralComparisons.StructuralEqualityComparer)
                    && ((GenericTypes == null && otherKey.GenericTypes == null)
                        || ((IStructuralEquatable)GenericTypes)?.Equals(otherKey.GenericTypes, StructuralComparisons.StructuralEqualityComparer) == true
                    );
			}
			return isEqual;
		}

		public override int GetHashCode()
		{
			int argHashes = 0;

			if (ArgTypes != null)
			{
				argHashes += ArgTypes
                    .Select((t, i) => (i + 1) * 7 + t.GetHashCode()).Sum();
			}

            if (GenericTypes != null)
            {
                argHashes += GenericTypes
                    .Select((t, i) => (i + 1) * 997 + t.GetHashCode()).Sum();
            }

            return ParentType.GetHashCode() + MethodName.GetHashCode() + argHashes;
		}
	}
}
