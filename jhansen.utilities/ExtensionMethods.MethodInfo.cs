﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JHansen.Utilities
{
    public static partial class ExtensionMethods
    {
	    public static bool TryConvertArguments(this MethodInfo method, Type target, string[] values)
	    {
			object[] converted;
		    return TryConvertArguments(method, target, values, out converted);
	    }

        public static bool IsMatch(this MethodInfo method, MethodSignatureKey key, string[] argValues)
        {

            string methodName = method.Name;
            return method.Name.EqualsIgnoreCase(key.MethodName)
                && method.TryConvertArguments(key.ParentType, argValues)
                && method.GetGenericArguments().Length == key.GenericTypes?.Length;
        }

        /// <summary>
        /// Takes a MethodInfo that is the open-generic version of a generic method (IsGenericMethodDefinition == true), and attempts to
        /// generate a closed-generic version using the given type names.
        /// </summary>
        /// <param name="typeNames">The names of types to be used in the closed generic version. Each string in the array
        /// will be used to find a type from the loaded assemblies by matching the end of a type's FullName string (namespace and class name).</param>
        /// <returns>The ready-to-execute closed-generic version of the given MethodInfo.</returns>
        public static MethodInfo MakeGenericMethod(this MethodInfo method, params string[] typeNames)
        {
            MethodInfo result = method;
            
            if (method?.IsGenericMethodDefinition == true)
            {
                Type[] genericArguments = method.GetGenericArguments();
                if (typeNames?.Length != genericArguments.Length)
                {
                    throw new InvalidOperationException(
                        $"The generic method {method.DeclaringType?.FullName}.{method.Name} requires {genericArguments.Length} type parameters. Given {nameof(typeNames)} '{string.Join(", ", typeNames??new string[0])}'.");
                }
                // scan the loaded assemblies and find the types named in the string that satisfy the generic constraints
                var scanResult = genericArguments
                    .Zip(typeNames, (def, name) => new {TypeParameter = (TypeInfo) def, NamedType = name})
                    .Select(
                        arg =>
                            new
                            {
                                Info = arg.TypeParameter,
                                Name = arg.NamedType,
                                FoundType = arg.TypeParameter.ScanAssembliesForMatchingTypes(arg.NamedType).FirstOrDefault()
                            })
                    .ToArray();

                if (scanResult.Any(sr => sr.FoundType == null))
                {
                    string missingTypes = string.Join(", ", scanResult
                        .Where(sr => sr.FoundType == null)
                        .Select(sr => $"[TypeParameter: {sr.Info.Name}, NamedType: {sr.Name}]"));
                    throw new ApplicationException(
                        $"Unable to find matching type(s) for generic method {method.DeclaringType?.FullName}.{method.Name}: {missingTypes}");
                }

                // close the generic type with the matching type(s)
                Type[] types = scanResult
                    .Select(sr => sr.FoundType)
                    .ToArray();
                result = method.MakeGenericMethod(types);
            }

            return result;
        }

		public static bool TryConvertArguments(this MethodInfo method, Type target, string[] values, out object[] convertedValues)
	    {
			bool success = false;
			List<object> converted = new List<object>();
			if (method != null)
			{
				ParameterInfo[] parameters = method.GetParameters();
			    Type firstParamType = parameters.FirstOrDefault()?.ParameterType;

                if (method.DeclaringType != target && firstParamType != null 
                    && firstParamType.IsReallyAssignableFrom(target))
				{
					// Extension method - skip the first parameter type when comparing the passed in args
					parameters = parameters.Skip(1).ToArray();
				}
				
				if (parameters.Count(p => !p.IsOptional) <= values.Length && parameters.Length >= values.Length)
				{
                    success = true;
                    // the provided number of arguments satisfies the number of parameters for this method
                    foreach (var arg in parameters.Zip(values, (p, a) => new { ParamType = p.ParameterType, ArgValue = a }))
					{
						try
						{
							// try to convert the arg string to the type of the method parameter
							converted.Add(arg.ArgValue.ConvertTo(arg.ParamType));
						}
						catch
						{
                            success = false;
							break; // can't use this method
						}
					}
				    if (success && converted.Count < parameters.Length)
				    {
				        // add placeholders for missing optional parameters
				        converted.AddRange(Enumerable.Repeat(Type.Missing, parameters.Length - converted.Count));
				    }
				}
			}

			convertedValues = converted.ToArray();
		    return success;
	    }
    }
}
