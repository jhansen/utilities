﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JHansen.Utilities.Diagnostics;

namespace JHansen.Utilities
{
	public static partial class ExtensionMethods
	{
		/// <summary>
		/// Merge Dictionaries, if Duplicate keys, only one of the values are kept
		/// </summary>        
		/// <typeparam name="K">Key Type</typeparam>
		/// <typeparam name="V">Value Type</typeparam>        
		/// <param name="others">Multiple Dictionaries accepted</param>
		/// <returns>Returns a merged dictionary</returns>
		public static T Merge<T, K, V>(this T me, params IDictionary<K, V>[] others) where T : IDictionary<K, V>, new()
		{
			foreach (IDictionary<K, V> src in (new List<IDictionary<K, V>> { me }).Concat(others))
			{
				if (src != null)
				{
					for (int i = 0; i < src.Count; i++)
					{
						KeyValuePair<K, V> p = src.ElementAt(i);
						me[p.Key] = p.Value;
					}
				}
			}
			return me;
		}

		/// <summary>
		/// Adds a key/value pair to the dictionary if the key does not already exist. 
		/// </summary>
		public static void AddIfNew(this IDictionary<string, string> dictionary, string key, string value)
		{
			if (!dictionary.ContainsKey(key))
			{
				dictionary.Add(key, value);
			}
		}

		public static bool TryGetValue(this IDictionary valueSource, string key, Type dictType, bool createIfMissing, out object value, out Type valueType)
		{
			valueType = (dictType ?? valueSource.GetType()).GetInterface("IDictionary`2")?.GetGenericArguments().LastOrDefault();
			bool success = false;
			value = null;

            if (valueSource.Contains(key))
			{
				value = valueSource[key];
				success = true;
			}
			else
            {
                // do a case-insensitive check
                foreach (var i in valueSource.Keys)
			    {
			        if (i.ToString().EqualsIgnoreCase(key))
			        {
			            value = valueSource[i];
                        success = true;
                        break;
			        }
			    }

			    if (!success)
			    {
			        if (createIfMissing)
			        {
			            // the dictionary doesn't contain the key, add it
			            value = valueType?.GetDefaultValue();

			            if (valueType != null)
			            {
			                valueSource[key] = value;
			                success = true;
			            }
			        }
                }
            }
			
			// if we weren't able to determine the value type by looking at the dictionary, take the type of the value itself
			if ((valueType == null || valueType == typeof (object)) && value != null)
			{
				valueType = value.GetType();
			}

			return success;
		}

		public static bool TrySetValue(this IDictionary dictionary, Type valueType, string key, object value)
		{
			bool success = false;
			try
			{
				dictionary[key] = value.ConvertTo(valueType);
				success = true;
			}
			catch (Exception ex)
			{
				_traceSource.TraceEvent(TraceEventType.Error, TraceEvent.DictionarySetValueException, ex);
			}
			return success;
		}
		
		public static bool TryGetValueGeneric<TValue>(this IDictionary<string, TValue> valueSource, string key, Type dictType, bool createIfMissing, out TValue value, out Type valueType)
	    {
            valueType = (dictType ?? valueSource.GetType()).GetInterface("IDictionary`2")?.GetGenericArguments().LastOrDefault();
	        bool success = false;

	        if (valueSource.ContainsKey(key))
	        {
	            value = valueSource[key];
	            success = true;
	        }
	        else
	        {
                // do a case-insensitive check
	            var item = valueSource.FirstOrDefault(i => i.Key.EqualsIgnoreCase(key));

	            if (!item.Equals(default(KeyValuePair<string, object>)))
	            {
                    value = item.Value;
                    success = true;
	            }
	            else if (createIfMissing)
	            {
	                // the dictionary doesn't contain the key, add it
	                value = (TValue)valueType?.GetDefaultValue();

	                if (valueType != null)
	                {
	                    valueSource[key] = value;
	                    success = true;
	                }
	            }
	            else
	            {
	                value = default(TValue);
	            }
            }
	        
	        // if we weren't able to determine the value type by looking at the dictionary, take the type of the value itself
	        if ((valueType == null || valueType == typeof(object)) && value != null)
	        {
	            valueType = value.GetType();
	        }

	        return success;
	    }

	    public static bool TrySetValueGeneric<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, Type valueType, TKey key, TValue value)
	    {
            bool success = false;
	        try
	        {
	            dictionary[key] = (TValue)value.ConvertTo(valueType);
	            success = true;
	        }
	        catch (Exception ex)
	        {
	            _traceSource.TraceEvent(TraceEventType.Error, TraceEvent.DictionarySetValueException, ex);
	        }
	        return success;
	    }
	}

}