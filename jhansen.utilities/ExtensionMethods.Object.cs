﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq.Dynamic;
using JHansen.Utilities.Diagnostics;
using JHansen.Utilities.Reflection;
using Newtonsoft.Json;

namespace JHansen.Utilities
{
	public static partial class ExtensionMethods
	{
		// Splitter regex used to split strings on periods (except for those inside of curly braces, angle brackets, parenthesis, or escaped with another period)
		private static readonly Regex Splitter = new Regex(@"(?<!\.)\.(?!([^\{]*\})|([^\<\{]*\>)|([^\(]*\))|\.)", RegexOptions.ExplicitCapture | RegexOptions.Compiled);
		// NameParts regex used to parse the different pieces of each property name (the parts between the periods)
		//private static readonly Regex NamePart = new Regex(@"^(?<member>\w+)(?<method>\((?<params>.+)?\))?(?>\[""?(?<key>\w+)""?\]|\{(?<where>.+)\}(?<listify>\[\])?)?(:(?<format>.+))?$", RegexOptions.Compiled | RegexOptions.ExplicitCapture);
		private static readonly Regex NamePart = new Regex(@"^(?<member>\w+)(\<(?<genericTypes>.+)?\>)?(?>(?<method>\((?<params>.+)?\))|(\[""?(?<key>\w+)""?\])|(\{(?<where>.+)\}))*(?<listify>\[\])?(:(?<format>.+))?$", RegexOptions.ExplicitCapture | RegexOptions.Compiled);
		// CrayParts regex is run against the entire propName (before splitting it) to determine if the more time-intensive Regex methods are needed on each name part
		private static readonly Regex CrayParts = new Regex(@"(?>(?<key>\[)|(?<where>\{)|(?<format>:)|(?<method>\())", RegexOptions.Compiled | RegexOptions.ExplicitCapture);

		public static PropertyEvaluationInfo GetPropertyEvalInfo(this Object obj, string propName,
			NamedParameters namedParameters = null, bool createChain = false)
		{
			PropertyEvaluationInfo result = new PropertyEvaluationInfo{ Source = propName };

			// Figure out whether we need to use the regex methods for this propName
			MatchCollection specialCharacters = CrayParts.Matches(propName);
			bool hasLamda = false, hasDictionary = false, hasFormat = false, hasMethod = false;
			foreach (Match m in specialCharacters)
			{
				hasDictionary = hasDictionary || m.Groups["key"].Success;
				hasLamda = hasLamda || m.Groups["where"].Success;
				hasFormat = hasFormat || m.Groups["format"].Success;
				hasMethod = hasMethod || m.Groups["method"].Success;
			}

			string[] nameParts = hasLamda || hasFormat || hasMethod
				? Splitter.Split(propName)
				: propName.Split('.');

			Type type = obj.GetType();
			bool evalFailed = false;
            Func<object, bool> setValue = null;

            if (type.Name == nameParts.First())
		    {
                nameParts = nameParts.Skip(1).ToArray();
            }

			for (int i = 0; i < nameParts.Length; i++)
			{
				if (obj == null)
					break;
                
				Type valueType = null;
				bool isEndOfChain = i == nameParts.Length - 1;
				string memberName;
			    string genericTypes = null;
				string dictKey = null;
				string lambda = null;
				string formatString = null;
				bool returnList = false;
				bool isMethod = false;
				string methodParameters = null;
				if (hasLamda || hasDictionary || hasFormat || hasMethod)
				{
					// The propName contains some dictionary or lambda parts, so use the Regex to find the pieces (slower)
					var match = NamePart.Matches(nameParts[i])[0];
					memberName = match.Groups["member"].Value;
                    genericTypes = match.Groups["genericTypes"].Value;
                    dictKey = match.Groups["key"].Value;
					lambda = match.Groups["where"].Value;
					returnList = !string.IsNullOrEmpty(match.Groups["listify"].Value);
					formatString = match.Groups["format"].Value;
					isMethod = !string.IsNullOrEmpty(match.Groups["method"].Value);
					methodParameters = match.Groups["params"].Value;
				}
				else
				{
					// The propName is known to not contain any dictionary or lambda parts, so the entire part is just a property name (faster)
					memberName = nameParts[i];
				}

				Func<object> getValue = null;
			    ReflectedMemberType memberType = ReflectedMemberType.None;

				if (isMethod)
				{
					MethodExecutionInfo methodInfo = type.GetMatchingMethod(memberName, methodParameters, genericTypes);
					if (methodInfo.SuccessfullyEvaluated)
					{
						object parent = obj;
						getValue = () => methodInfo.Execute(parent);
						valueType = methodInfo.ReturnType;
					    memberType = ReflectedMemberType.Method;
					}
					else
					{
						evalFailed = true;
					}
				}
				else
				{
					PropertyInfo prop = type.GetProperty(memberName);

					if (prop == null)
					{
						// No matching property found; check to see if the object is a dictionary and this is a key
						var dict = obj as IDictionary;
						object dictionaryEntry = null;
						if (dict?.TryGetValue(memberName, type, createChain, out dictionaryEntry, out valueType) != true)
						{
							break;
						}
						getValue = () => dictionaryEntry;
						Type closureValueType = valueType;
						setValue = v => dict.TrySetValue(closureValueType, memberName, v);
					    memberType = ReflectedMemberType.Dictionary;
					}
					else
					{
						valueType = prop.PropertyType;
						PropertyInfo closureProp = prop;
						object parent = obj;
						getValue = () => closureProp.GetValue(parent, createChain);
					    memberType = ReflectedMemberType.Property;
						if (closureProp.CanTrySetValue())
						{
							setValue = v => closureProp.TrySetValue(parent, v);
						}
					}
				}

				object valueOverride = null;
				// ReSharper disable AccessToModifiedClosure
				Func<object> currentValue = () => valueOverride ?? getValue?.Invoke();
				// ReSharper restore AccessToModifiedClosure
				
				if (!string.IsNullOrEmpty(dictKey))
				{
					// this part of the string is accessing a dictionary by key
				    var currentValueResult = currentValue();
				    getValue = null;
                    memberType = ReflectedMemberType.Dictionary;

                    var dict = currentValueResult as IDictionary;

				    if (dict != null)
				    {
				        // Get the item from the dictionary (adding it if it doesn't exist and createChain == true)
				        evalFailed = dict?.TryGetValue(dictKey, valueType, createChain, out valueOverride, out valueType) != true;
				        if (!evalFailed)
				        {
                            obj = dict;
				            Type closureValueType = valueType;
				            setValue = v => dict.TrySetValue(closureValueType, dictKey, v);
				        }
                    }
				    else
				    {
                        // check if the value is an IDictionary<string, object> (ExpandoObject doesn't implmenent the non-generic IDictionary)
				        var genericDict = currentValueResult as IDictionary<string, object>;
				        evalFailed = genericDict?.TryGetValueGeneric(dictKey, valueType, createChain, out valueOverride, out valueType) != true;
				        if (!evalFailed)
				        {
				            obj = genericDict;
				            Type closureValueType = valueType;
				            setValue = v => genericDict.TrySetValueGeneric(closureValueType, dictKey, v);
				        }
                    }
				}

				if (!string.IsNullOrEmpty(lambda))
				{
					// this part of the string refers to an item in a list with a "where" clause lambda
					var value = currentValue();
					getValue = null;
                    setValue = null; // you can't set the value when the value is the result of a where clause

                    // make note of the type of values in the list
                    valueType = returnList
						? valueType
						: valueType.GetGenericArguments().LastOrDefault();
					var queryable = value as IQueryable;
					if (queryable == null)
					{
						var enumerable = value as IEnumerable;
						if (enumerable != null)
						{
							queryable = enumerable.AsQueryable();
						}
					}

					if (queryable == null)
						break;

					try
					{
						// execute the dynamic linq Where clause, and grab the first matching item
						valueOverride = queryable.Where(lambda, namedParameters).Cast<object>();
					    if (!returnList)
					    {
					        valueOverride = ((IQueryable<object>) valueOverride).FirstOrDefault();
                            memberType = ReflectedMemberType.FilteredListItem;
                        }
					    else
					    {
                            memberType = ReflectedMemberType.FilteredList;
                        }
					}
					catch (ParseException ex)
					{
						valueOverride = null;
						getValue = null;
						result.Error = ex.GetSummary();
						_traceSource.TraceEvent(TraceEventType.Error, TraceEvent.DynamicLinqParseException, ex);
					}
				}

				if (!isEndOfChain)
				{
					obj = currentValue();
                    type = valueType == typeof(object) && obj != null
                        ? obj.GetType() // try to get more specific type
                        : valueType;
				} else
				{
					// end of the road, made it all the way to the end of the propName string
					result.SuccessfullyEvaluated = obj != null && !evalFailed;
					result.Parent = obj;
					result.ValueType = valueType;
					result.GetRawValue = currentValue ?? (() => null);
					result.TrySetValue = evalFailed ? null : setValue;
					result.FormatString = formatString?.Replace("..", "."); // replace double (escaped) periods with a single period
				    result.MemberType = memberType;
				}
			}

			return result;
		}

		public static object GetPropertyValue(this object obj, string propName)
		{
			PropertyEvaluationInfo info = obj.GetPropertyEvalInfo(propName);

		    return info.GetValue();
		}
		
		/// <summary>
		/// Returns true if the given object is a numeric type with a value equal to zero, or if the object is the string "0",
		/// otherwise false.
		/// </summary>
		public static bool IsZero(this object obj)
		{
			if (obj == null)
				return false;
			double number;
			return double.TryParse(Convert.ToString(obj, CultureInfo.InvariantCulture), NumberStyles.Any, NumberFormatInfo.InvariantInfo, out number) && number.CompareTo(0) == 0;
		}

	}
}
