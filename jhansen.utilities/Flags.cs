﻿using System.Reflection;

namespace JHansen.Utilities
{
	public sealed class Flags
	{
		public const BindingFlags PublicMemberBindingFlags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.Static;
		public const BindingFlags PublicConstructorBindingFlags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.CreateInstance;
		public const BindingFlags AllMemberBindingFlags = PublicMemberBindingFlags | BindingFlags.NonPublic | BindingFlags.InvokeMethod;
		public const BindingFlags AllMethodBindingFlags = AllMemberBindingFlags;
		public const BindingFlags AllPropertyBindingFlags = AllMemberBindingFlags | BindingFlags.GetProperty | BindingFlags.SetProperty;
		public const BindingFlags AllFieldBindingFlags = AllMemberBindingFlags | BindingFlags.GetField | BindingFlags.SetField;
		public const BindingFlags AllConstructorBindingFlags = PublicConstructorBindingFlags | BindingFlags.NonPublic;
	}
}
