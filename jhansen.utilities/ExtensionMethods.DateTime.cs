﻿using System;

namespace JHansen.Utilities
{
	public static partial class ExtensionMethods
	{
		public static DateTime ConvertFromUtc(this DateTime utcDate, TimeZoneInfo timeZoneInfo)
		{
			var localDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcDate, timeZoneInfo);
			return DateTime.SpecifyKind(localDateTime, DateTimeKind.Local);
		}

		/// <summary>
		/// Converts a timespan from UTC to the equivalent time in the given timezone.
		/// </summary>
		/// <param name="utcTime">Value to convert.</param>
		/// <param name="timeZoneInfo">Timezone to convert to.</param>
		/// <param name="date">Used when converting timezones; this matters because of daylight savings time. If not specified, 
		/// the current date will be used.</param>
		public static TimeSpan ConvertFromUtc(this TimeSpan utcTime, TimeZoneInfo timeZoneInfo, DateTime? date = null)
		{
			DateTime baseDate = date.HasValue
				? date.Value.Date
				: DateTime.UtcNow.Date;

			return new DateTime(baseDate.Ticks + utcTime.Ticks).ConvertFromUtc(timeZoneInfo).TimeOfDay;
		}

		/// <summary>
		/// Converts a timepan in a given timezone to the UTC equivalent.
		/// </summary>
		/// <param name="time">Value to convert</param>
		/// <param name="timeZoneInfo">Timezone that the given value is in</param>
		/// <param name="date">Used when converting timezones; this matters because of daylight savings time. If not specified, 
		/// the current date will be used.</param>
		public static TimeSpan ToUniversalTime(this TimeSpan time, TimeZoneInfo timeZoneInfo, DateTime? date = null)
		{
			DateTime baseDate = date.HasValue
				? date.Value.Date
				: DateTime.UtcNow.Date;

			return TimeZoneInfo.ConvertTimeToUtc(new DateTime(baseDate.Ticks + time.Ticks), timeZoneInfo).TimeOfDay;
		}

        public static DateTime ToUniversalTime(this DateTime original, DateTimeKind defaultKind)
		{
			return original.Kind == DateTimeKind.Unspecified
				? DateTime.SpecifyKind(original, defaultKind).ToUniversalTime()
				: original.ToUniversalTime();
		}

		public static DateTime? ToUniversalTime(this DateTime? original, DateTimeKind defaultKind)
		{
			return original.HasValue
				? (DateTime?)original.Value.ToUniversalTime(defaultKind)
				: null;
		}

        public static DateTime ToUniversalTime(this DateTime original, TimeZoneInfo timeZone)
        {
            return TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(original, DateTimeKind.Unspecified), timeZone);
        }
        
        public static DateTime? ToUniversalTime(this DateTime? original, TimeZoneInfo timeZone)
        {
            return original.HasValue 
                ? (DateTime?)TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(original.Value, DateTimeKind.Unspecified), timeZone)
                : null;
        }
    }
}
