﻿using System;
using System.Reflection;

namespace JHansen.Utilities
{
	public static class EnumHelper
	{
		static readonly MethodInfo EnumTryParse;

		static EnumHelper()
		{
			EnumTryParse = typeof(Enum)
				.GetMethods(BindingFlags.Public | BindingFlags.Static)
				.First(m => m.Name == "TryParse" && m.GetParameters().Length == 3);
		}

		/// <summary>
		/// A non-generic version of Enum.TryParse(). This can be removed if they ever add it to .NET.
		/// </summary>
		public static bool TryParse(
			Type enumType,
			string value,
			bool ignoreCase,
			out object enumValue)
		{
			MethodInfo genericEnumTryParse = EnumTryParse.MakeGenericMethod(enumType);

			object[] args = new object[] { value, ignoreCase, Enum.ToObject(enumType, 0) };
			bool success = (bool)genericEnumTryParse.Invoke(null, args);
			enumValue = args[2];

			return success;
		}
	}
}
