﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace JHansen.Utilities
{
    public partial class ExtensionMethods
    {
		private static readonly Lazy<List<Type>> ExportedTypes = new Lazy<List<Type>>(() => AppDomain.CurrentDomain.GetAssemblies()
            .Where(a => !a.IsDynamic) // exclude auto-generated assemblies
            .SelectMany(a => a.GetExportedTypes())
			.ToList());

		/// <summary>
        /// Gets all assemblies loaded in the current AppDomain (excluding dynamic assemblies) and calls <see cref="Assembly.GetExportedTypes"/>
        /// for each, returning the superset of all Types. The list is evaluated once and staticly-cached for subsequent calls.
        /// </summary>
        public static List<Type> GetExportedTypes(this AppDomain appDomain)
        {
            return ExportedTypes.Value;
        }
    }
}
