﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using JHansen.Utilities.Reflection;

namespace JHansen.Utilities
{
	public static partial class ExtensionMethods
	{
		/// <summary>
		/// Compares two strings while ignoring case. Checks for null
		/// </summary>
		/// <param name="s2">string to compare to</param>
		public static bool EqualsIgnoreCase(this string s1, string s2)
		{
			return s1.EqualsIgnoreCase(s2, false);
		}

		/// <summary>
		/// Compares two strings while ignoring case. Has a boolean flag when true 
		/// trims any whitespace around the strings prior to the comparison, checks for null
		/// </summary>
		/// <param name="s2">string to compare to</param>
		/// <param name="trimWhitespace">true trims whitespace, false does not</param>
		public static bool EqualsIgnoreCase(this string s1, string s2, bool trimWhitespace)
		{
			bool equal = false;

			if (s1 != null && s2 != null)
			{
				string a = s1;
				string b = s2;

				if (trimWhitespace)
				{
					a = a.Trim();
					b = b.Trim();
				}

				equal = a.Equals(b, StringComparison.CurrentCultureIgnoreCase);
			}

			return equal;
		}
		
		public static string GetValueOrDefault(this string value)
		{
			if (value == null)
			{
				return string.Empty;
			}
			return value;
		}
		
		/// <summary>
		/// Checks for Null, Trims and Returns True if length > 0
		/// </summary>
		public static bool IsNullOrEmptyTrimmed(this string value)
		{
			if (value != null && value.Trim().Length > 0)
				return false;
			else
				return true;
		}

		public static bool IsNumeric(this string value)
		{
			return Regex.IsMatch(value, @"^\d+$");
		}

		/// <summary>
		/// Converts a string to the enumeration specified in the type parameter.
		/// </summary>
		/// <typeparam name="T">The enumeration type to convert.</typeparam>
		/// <param name="value">The string value to be converted.</param>
		/// <returns>An enumeration value that corresponds to the specified string.</returns>
		public static T ToEnum<T>(this string value) where T : struct, IConvertible
		{
			if (!typeof(T).IsEnum)
			{
				throw new ArgumentException("T must be an enumerated type");
			}
						
			try
			{
				return (T)Enum.Parse(typeof(T), value);
			}
			catch
			{
				return default(T);
			}			
		}
		
		private static readonly Regex HumpsRegex = new Regex("([^A-Z])([A-Z])", RegexOptions.Compiled);

		/// <summary>
		/// Takes a Pascal- or Camel-case string and turns it into a hyphenated lower-case string
		/// </summary>
		public static string ToHyphenatedString(this string humps)
		{
			return humps != null
				? HumpsRegex.Replace(humps, m => string.Format("{0}-{1}", m.Groups[1].Value, m.Groups[2].Value)).ToLower()
				: null;
        }
		
		public static bool IsDate(this string value)
		{
			DateTime dummyDate;
			return DateTime.TryParse(value, out dummyDate);
		}
		
		private static readonly Regex ControlCharacters = new Regex(@"(?![\r\n])\p{Cc}", RegexOptions.Compiled);

		public static string RemoveControlCharacters(this string input)
		{
			return ControlCharacters.Replace(input, "");
		}

		public static string JoinNonEmpty(this string separator, params string[] values)
		{
			return string.Join(separator, values.Where(v => !string.IsNullOrWhiteSpace(v)));
		}

		public static string TrimEnd(this string source, string value, StringComparison comparisonType = StringComparison.Ordinal)
		{
			return source != null && source.EndsWith(value) 
				? source.Remove(source.LastIndexOf(value, comparisonType)) 
				: source;
		}
		
		/// <summary>
		/// Convenience overload that takes just a single object rather than an IEnumerable.
		/// </summary>
		public static string ReplaceTokens(this string source, object obj = null, IDictionary flatValues = null, Func<Match, string> invalidTokenHandler = null)
		{
			object[] objects = obj != null
				? new[] { obj }
				: null;
			return source.ReplaceTokens(objects, flatValues, invalidTokenHandler);
		}
		
		public static readonly Regex LiquidTokenRegex = new Regex("{{(?<token>.*?)}}", RegexOptions.Compiled);
		/// <summary>
		/// Looks for and replaces tokens in a string with their corresponding values found in the given objects.
		/// </summary>
		/// <typeparam name="T">Generally this is just object, but it is the type of items in <paramref name="objects"/></typeparam>
		/// <param name="source">The string in which tokens reside. They will be eliminated with extreme prejudice. Tokens are of the Liquid variety - {{SomeObject.Property}}</param>
		/// <param name="objects">A list of objects to go spelunking in when trying to resolve token values.</param>
		/// <param name="flatValues">A dictionary of other values to use as token replacements. If given, tokens representing keys from this dictionary will be replaced with the corresponding value.</param>
		/// <param name="invalidTokenHandler">A func that will be invoked for each token that cannot be resolved using the standard reflection logic. The string returned here will replace the token.</param>
		/// <returns>The original string with tokens replaced.</returns>
		public static string ReplaceTokens<T>(this string source, IEnumerable<T> objects = null, IDictionary flatValues = null, Func<Match, string> invalidTokenHandler = null)
		{
			if (!string.IsNullOrEmpty(source))
			{
				source = LiquidTokenRegex.Replace(source, m => {
					string token = m.Groups["token"].Value;
					PropertyEvaluationInfo tokenEvalInfo = objects.GetPropertyEvalInfo(token, flatValues);
					if (tokenEvalInfo.SuccessfullyEvaluated)
					{
						return tokenEvalInfo.GetValue()?.ToString();
					}
					return invalidTokenHandler?.Invoke(m) ?? m.Value;
				});
			}

			return source;
		}
		
	}
}